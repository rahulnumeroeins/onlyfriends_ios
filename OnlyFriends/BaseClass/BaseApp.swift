//
//  BaseApp.swift
//  PrivateLocator
//
//  Created by Admin on 28/10/20.
//  Copyright © 2020 Gaurav Kive. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

struct DeviceInfo{
    
    var plateform = ""
    var deviceModel = ""
    var osName = ""
    var osVersion = ""
    var locale = ""
    var timeZone = ""
    var advertisingID = ""
    var deviceName = ""
}

//MARK:-  NSObject Life Cycle
//MARK:-
class BaseApp: NSObject {
    //Variable declaration
    static let sharedInstance = BaseApp()
    static let appDelegate    = UIApplication.shared.delegate as! AppDelegate
    static let appWindow      = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
    
    //
    var deviceInfo: DeviceInfo?
    var jobManager: OperationQueue?

    //
    var isLocationServiceAlertDisplay = false
    var locationAlert:UIAlertController?

    //MARK: - Basic Init setup
    override init () {
        super.init()
        initJobManager()
        //reachability = try! Reachability()
        deviceInfo = getDeviceInfo()
    }
    
    func initJobManager() {
        jobManager = OperationQueue()
        jobManager?.maxConcurrentOperationCount = 50
        jobManager?.qualityOfService = QualityOfService.background
    }
}

extension BaseApp {
    // Get Device info
    func getDeviceInfo() -> DeviceInfo{
        // common device parameters
        let device = UIDevice.current
        var model = DeviceInfo()
        
        model.plateform = device.systemName
        model.deviceModel = device.model
        model.osName = device.systemName
        model.osVersion = device.systemVersion
        model.locale = Locale.current.languageCode!
        model.timeZone = TimeZone.current.abbreviation()!
        model.advertisingID = device.identifierForVendor!.uuidString
        model.deviceName = device.name
        
        return model
    }
    
    func removeOptionalString(_ convertText:String) -> String{
        var convertedString = convertText
        convertedString = convertedString.replacingOccurrences(of: "Optional(", with: "")
        convertedString = convertedString.replacingOccurrences(of: ")", with: "")
        return convertedString
    }
    
    func getMessageForCode(_ constantName: String) -> String? {
        let fileName = DefaultLang
        return NSLocalizedString(constantName, tableName: fileName, bundle: Bundle.main, value: "", comment: "")
    }
    
    func toString(_ object: AnyObject?) -> String?{
        return String(format: "%@", object as! NSString)
    }
}

extension BaseApp {
    func setAuthenticationToken() -> [String:String] {
        //Get Header Parameters
        let authorization  = ApplicationPreference.sharedManager.read(type: PreferenceType.authorization) as! String
        return [kAuthorization : authorization]
    }
    
    //MARK:- Check if user if already logged in to application
    func isUserAlreadyLoggedInToApplication() -> Bool {
        if (ApplicationPreference.sharedManager.read(type: .socialLogin) as AnyObject).boolValue == true {
            return true
        } else {
            if (ApplicationPreference.sharedManager.read(type: .mobile_verify) as AnyObject).boolValue == true {
                return true
            } else {
                return false
            }
        }
    }

    //MARK:- Check if  App Launch FirstTime
    func isAppLaunchFirstTime() -> Bool {
        let appLaunchInfo = ApplicationPreference.sharedManager.read(type: .appLaunchFirstTime) as! String
        if appLaunchInfo != "" {
            if appLaunchInfo == "YES" {
                return true
            } else {
                return false
            }
        }
        return false
    }
    
    //MARK:- Check to show Tutorial
    func isShowTutorial() -> Bool {
        let showTutorial = ApplicationPreference.sharedManager.read(type: .showTutorial) as! String
        if showTutorial != "" {
            if showTutorial == "YES" {
                return true
            } else {
                return false
            }
        }
        return false
    }
}

// constants
let APPLE_LANGUAGE_KEY = "AppleLanguage"
/// AppLanguage

class LanguageMethods {
    
    fileprivate static let defaults = UserDefaults.standard
    
    /// get current Apple language
    class func currentAppleLanguage() -> String{
        let langArray = defaults.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        let currentWithoutLocale = current.stringFrom(0, to: 2)
        return currentWithoutLocale
    }
    
    class func currentAppleLanguageFull() -> String{
        let langArray = defaults.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        return current
    }
    
    /// set @lang to be the first in Applelanguages list
    class func setApplicationLanguageTo(lang: String) {
        defaults.removeObject(forKey: APPLE_LANGUAGE_KEY)
        defaults.set([lang], forKey: APPLE_LANGUAGE_KEY)
        defaults.synchronize()
    }
    
    class var isRTL: Bool {
        return LanguageMethods.currentAppleLanguage() == AppLanguage.English.LanguageCode
    }
}

//MARK:- ======== Implement logged-in/logout functionality =======
//MARK:-
extension BaseApp{
    
    func openDashboardViewController(){
        //BaseApp.appDelegate.setUpRootViewController()
    }
    
    func openLoginViewController(){
        ApplicationPreference.sharedManager.clearDataOnLogout()
        //BaseApp.appDelegate.setUpInitialViewController()
    }
}

extension BaseApp {
    
    /**
     * For showing error dialog if result from an event is false
     * @param title Dialog Title
     * @param error Error message
     */
    
    func showAlertNativeDialog(title:String, message:String) {
        let networkAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let defaultAction = UIAlertAction(title: LocalizationKeys.btn_ok.getLocalized().capitalized
            , style: .default, handler: nil)
        networkAlert.addAction(defaultAction)
        BaseApp.appWindow?.rootViewController?.present(networkAlert, animated: true, completion: nil)
    }
    
    func showAlertNativeDialog(title:String, message:String,controller: UIViewController?) {
        let networkAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let defaultAction = UIAlertAction(title: LocalizationKeys.btn_ok.getLocalized().capitalized
            , style: .default, handler: nil)
        networkAlert.addAction(defaultAction)
        
        if controller != nil {
            controller?.present(networkAlert, animated: true, completion: nil)
        } else {
            BaseApp.appWindow?.rootViewController?.present(networkAlert, animated: true, completion: nil)
        }
    }
    
    func showAlertNativeDialog(title:String, message:String,completion:@escaping ((String)->Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: LocalizationKeys.btn_ok.getLocalized().capitalized, style: .default){ (action) in
            completion(LocalizationKeys.btn_ok.getLocalized())
        }
        alert.addAction(okAction)
        BaseApp.appWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func showLogOutAlertNativeDialog(title:String, message:String,completion:@escaping ((String)->Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let cancelAction = UIAlertAction(title: LocalizationKeys.btn_no.getLocalized().capitalized, style: .destructive){ (action) in
            completion(LocalizationKeys.btn_no.getLocalized().capitalized)
        }
        let okAction = UIAlertAction(title: LocalizationKeys.btn_yes.getLocalized().capitalized, style: .default){ (action) in
            completion(LocalizationKeys.btn_yes.getLocalized().capitalized)
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        BaseApp.appWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func showAlertNativeDialogWithSingleButton(title:String, message:String, btnTitleOne: String?, completion:@escaping ((String)->Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: btnTitleOne?.capitalized, style: .default){ (action) in
            completion((btnTitleOne)!)
        }
        alert.addAction(okAction)
        BaseApp.appWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func showAlertNativeDialogWithButtons(title:String?, message:String?, btnTitleOne: String?, btnTitleTwo: String?, completion:@escaping ((String)->Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: btnTitleOne?.capitalized, style: .default){ (action) in
            completion((btnTitleOne)!)
        }
        let cancelAction = UIAlertAction(title: btnTitleTwo?.capitalized, style: .destructive){ (action) in
            completion((btnTitleTwo)!)
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        BaseApp.appWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}
//MARK:- Check Device location service status
//MARK:-
extension BaseApp {
    
    // Display location disable alert
    func checkLocationServiceStatus(){
        
        if(!isLocationServiceAlertDisplay){
            isLocationServiceAlertDisplay = true
            if(BaseApp.sharedInstance.isDeviceLocationServiceOn() == false){
                let errorTitle = LocalizationKeys.app_name.getLocalized()
                let errorMsg = LocalizationKeys.msg_location_turned_off.getLocalized() + " " + LocalizationKeys.app_name.getLocalized()
                //show alert to cancel trip before trip start
                self.locationAlert = UIAlertController(title:errorTitle, message: errorMsg, preferredStyle: UIAlertController.Style.alert)
                self.locationAlert!.addAction(UIAlertAction(title:LocalizationKeys.btn_cancel.getLocalized(), style: UIAlertAction.Style.default, handler:{(action:UIAlertAction) in
                    self.isLocationServiceAlertDisplay = false
                    self.checkLocationServiceStatus()
                }))
                self.locationAlert!.addAction(UIAlertAction(title:LocalizationKeys.title_settings.getLocalized(), style: UIAlertAction.Style.default, handler:{(action:UIAlertAction) in
                    self.isLocationServiceAlertDisplay = false
                    self.openExternalWebViewWithURL(UIApplication.openSettingsURLString)
                }))
                BaseApp.appWindow?.rootViewController!.present(self.locationAlert!, animated: true, completion: nil)
            }
        }
    }
    
    func openExternalWebViewWithURL(_ url:String){
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: url)!)
        }
    }
    
    //Check Location service
    func isDeviceLocationServiceOn() -> Bool{
        var isFound = false
        
        if(CLLocationManager.locationServicesEnabled() == true && CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse){
            isFound = true
        }
        
        return isFound
    }
}

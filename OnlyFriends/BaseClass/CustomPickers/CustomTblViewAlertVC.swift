//
//  CustomTblViewAlertVC.swift
//  BeachButler
//
//  Created by NumeroEins on 12/12/20.
//  Copyright © 2020 NumeroEins. All rights reserved.
//

import UIKit

class CustomTblViewAlertVC: BaseAlertViewController {
 
    @IBOutlet weak var lblHeader:UILabel!
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var searchTxtField:UITextField!

    var callbackGetSelectedData : ((_ comment: String)->()) = {_ in}
    var arrData: [String] = []
    var arrFilteredData: [String] = []
    var strSelectedData: String = ""
    var strHeader: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.arrFilteredData = self.arrData
        
        self.lblHeader.text = "\(strHeader)"
        self.searchTxtField.addTarget(self, action: #selector(searchSports(_ :)), for: .editingChanged)
        self.searchTxtField.setLeftPadding(10)
        self.searchTxtField.setRightPadding(10)

        self.tblView.tableFooterView = UIView()
        self.tblView.reloadData()
    }
}

//==================================================
// MARK:- Action methods
//==================================================
extension CustomTblViewAlertVC {
    
    @IBAction func actionClose(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionDone(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
      
//        if strSelectedData == "" && arrFilteredData.count > 0 {
//            self.strSelectedData = self.arrFilteredData[0].trim()
//        }
        
    }
}

extension CustomTblViewAlertVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFilteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CustomTblViewCell = tableView.dequeueReusableCell(withIdentifier: "CustomTblViewCell", for: indexPath) as! CustomTblViewCell
        cell.lblTitle.text = self.arrFilteredData[indexPath.row].capitalized
        return cell
    }
}

extension CustomTblViewAlertVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.arrFilteredData.count > 0 {
            self.strSelectedData = self.arrFilteredData[indexPath.row].trim()
            self.dismiss(animated: true, completion: nil)
            self.callbackGetSelectedData(strSelectedData)
        }
    }
}

extension CustomTblViewAlertVC {
    @objc func searchSports(_ textfield:UITextField) {
        self.arrFilteredData = []
        if textfield.text?.count != 0 {
            self.arrFilteredData = self.arrData.filter{ (item) -> Bool in
                let stringMatch = item.lowercased().range(of:textfield.text!.lowercased())
                return stringMatch != nil ? true : false
            }
        } else {
            self.arrFilteredData = self.arrData
        }
        self.tblView.reloadData()
    }
}

class CustomTblViewCell : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
}

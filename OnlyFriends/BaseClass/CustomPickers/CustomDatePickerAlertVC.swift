//
//  CustomDatePickerAlertVC.swift
//  PartTime
//
//  Created by NumeroEins on 23/11/20.
//

import UIKit

enum DatePickerType{
    case DateTime
    case Date
    case Time
}

class CustomDatePickerAlertVC: BaseAlertViewController {
    
    @IBOutlet var lbl_CalenderHeader: UILabel!
    @IBOutlet var dataPicker: UIDatePicker!
    
    var preSelectedDate = Date()
    var selectDate = ""
    var datePickerType:DatePickerType = .DateTime
    var minimumDate = Date().adding(days: -1);
    var maximumDate = Date().adding(days: -1);
    
    var callbackGetSelectedData : (String, Date) ->() = {_,_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.4, *) {
            dataPicker.preferredDatePickerStyle = .wheels
        } else {
        }
        
        let formatter = DateFormatter()
        
        switch datePickerType {
        case .DateTime:
            
            dataPicker.datePickerMode = .dateAndTime
            dataPicker.date = preSelectedDate
            dataPicker.minimumDate = minimumDate
            dataPicker.maximumDate = maximumDate
            
            formatter.dateFormat = "dd-MMM-YYYY hh:mm:ss a"
            lbl_CalenderHeader.text = "Select Date & Time"
            break;
            
        case .Date:
            print("Date")
            dataPicker.datePickerMode = .date
            dataPicker.date = preSelectedDate
            dataPicker.minimumDate = minimumDate
            dataPicker.maximumDate = maximumDate
            
            formatter.dateFormat = "EEEE, dd-MMM-YYYY"
            
            lbl_CalenderHeader.text = "Select Date"
            
            break;
            
        case .Time:
            print("Time")
            dataPicker.datePickerMode = .time
            dataPicker.date = preSelectedDate
            dataPicker.minimumDate = minimumDate
            
            formatter.dateFormat = "HH:MM"
            lbl_CalenderHeader.text = "Select Time"
            break;
        }
    }
    
    func selected_Date() {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd-MMM-YYYY"
        selectDate = formatter.string(from: dataPicker.date)
        lbl_CalenderHeader.text = selectDate
    }
}

//==================================================
// MARK:- Action methods
//==================================================
extension CustomDatePickerAlertVC {
    
    @IBAction func datePicker_ValueChange(_ sender: Any) {
        selected_Date()
    }
    
    @IBAction func actionClose(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionDone(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
        
        let format = DateFormatter()
        format.dateFormat = "dd MMM yyyy, hh:mm a"
        selectDate = format.string(from: dataPicker.date)
        dataPicker.reloadInputViews()
        let timeStamp = dataPicker.date.timeIntervalSince1970
        let strTimeStamp = timeStamp * 1000
        //self.callbackGetSelectedData("\(Int(timeStamp)/1000)", dataPicker.date)
        self.callbackGetSelectedData(String(format: "%.0f", strTimeStamp), dataPicker.date)
    }
}

//
//  CustomPickerAlertVC.swift
//  PartTime
//
//  Created by NumeroEins on 23/11/20.
//

import UIKit

class CustomPickerAlertVC: BaseAlertViewController {
 
    @IBOutlet weak var lblHeader:UILabel!
    @IBOutlet weak var pickerVW:UIPickerView!
    @IBOutlet weak var searchTxtField:UITextField!

    var callbackGetSelectedData : ((_ comment: String)->()) = {_ in}
    var arrPickerData: [String] = []
    var arrFilteredPickerData: [String] = []
    var strSelectedData: String = ""
    var strHeader: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.arrFilteredPickerData = self.arrPickerData
        
        self.lblHeader.text = "\(strHeader)"
        self.searchTxtField.addTarget(self, action: #selector(searchSports(_ :)), for: .editingChanged)
        self.searchTxtField.setLeftPadding(10)
        self.searchTxtField.setRightPadding(10)

        self.pickerVW.reloadAllComponents()
    }
}

//==================================================
// MARK:- Action methods
//==================================================
extension CustomPickerAlertVC {
    
    @IBAction func actionClose(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionDone(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
      
        if strSelectedData == "" && arrFilteredPickerData.count > 0 {
            self.strSelectedData = self.arrFilteredPickerData[0].trim()
        }
        
        self.callbackGetSelectedData(strSelectedData)
    }
}

extension CustomPickerAlertVC: UIPickerViewDelegate {
    
}

extension CustomPickerAlertVC: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrFilteredPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.arrFilteredPickerData[row].capitalized
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.strSelectedData = self.arrFilteredPickerData[row].trim()
    }
}

extension CustomPickerAlertVC {
    @objc func searchSports(_ textfield:UITextField) {
        self.arrFilteredPickerData = []
        if textfield.text?.count != 0 {
            self.arrFilteredPickerData = self.arrPickerData.filter{ (item) -> Bool in
                let stringMatch = item.lowercased().range(of:textfield.text!.lowercased())
                return stringMatch != nil ? true : false
            }
        } else {
            self.arrFilteredPickerData = self.arrPickerData
        }
        self.pickerVW.reloadAllComponents()
    }
}

//
//  WebViewVC.swift
//  BeachButler
//
//  Created by NumeroEins on 21/12/20.
//  Copyright © 2020 NumeroEins. All rights reserved.
//

import UIKit
import WebKit

enum ScreenTitle : String {
    case about_us
    case help
    case settings
    case privacy_policy
    case term_condition
    case faq
}

class WebViewVC: BaseViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: WKWebView?
    @IBOutlet weak var btnMenu: UIButton?
    @IBOutlet weak var btnBack: UIButton?

    var screenTitle : ScreenTitle!
    var strViewComeFrom: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Spinner.show("")
        self.methodTosetLocalizedString()
        
        self.webView?.uiDelegate = self
        self.webView?.navigationDelegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.webView?.isOpaque = false
        self.webView?.backgroundColor = UIColor.clear
        self.webView?.scrollView.backgroundColor = UIColor.clear
        self.webView?.layer.backgroundColor = UIColor.clear.cgColor
    }
    
    fileprivate func methodTosetLocalizedString(){
      
        if strViewComeFrom == "SideMenu" {
            self.btnMenu?.isHidden = false
            self.btnBack?.isHidden = true
        } else {
            self.btnMenu?.isHidden = true
            self.btnBack?.isHidden = false
        }
        
        var strURL = ""
                
        switch screenTitle {
            
        case .about_us?:
            lbl_NavigationTitle?.text = LocalizationKeys.title_about_the_app.getLocalized()
            strURL = "https://arthros.clinic/about/"
            break
            
        case .help:
            lbl_NavigationTitle?.text = LocalizationKeys.title_help.getLocalized()
            strURL = "https://www.google.com"
            break
            
        case .settings:
             lbl_NavigationTitle?.text = LocalizationKeys.title_settings.getLocalized()
            break
            
        case .privacy_policy?:
            lbl_NavigationTitle?.text = LocalizationKeys.title_privacy_policy.getLocalized()
            strURL = "https://www.google.com"
            break
            
        case .term_condition?:
            lbl_NavigationTitle?.text = LocalizationKeys.title_terms_conditions_settings.getLocalized()
            strURL = "https://www.google.com"
            break
            
        case .faq?:
            lbl_NavigationTitle?.text = LocalizationKeys.title_help.getLocalized()
            strURL = "https://www.google.com"
            break
            
        default:
            break
        }
        
        let url = URL (string: strURL)
        let req = URLRequest.init(url: url!)
        self.webView?.load(req)
        
        //let content = "<html><body><p><font size=30>" + strURL + "</font></p></body></html>"
        //self.webView?.loadHTMLString(content, baseURL: nil)
    }
}

//==========================================
//MARK:- UIWebViewDelegate
//==========================================
extension WebViewVC: WKUIDelegate, WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Spinner.hide()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
}

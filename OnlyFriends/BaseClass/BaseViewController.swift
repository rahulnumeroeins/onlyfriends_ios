//
//  BaseViewController.swift
//  ARMap
//
//  Created by NumeroEins on 19/02/21.
//  Copyright © 2021 NumeroEins. All rights reserved.
//

let screenSize = UIScreen.main.bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height
let WIDTH_FACTOR = screenWidth/320
let HEIGHT_FACTOR = screenHeight/568

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    @IBOutlet weak var lbl_NavigationTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.view.getViewsOf(type: UIButton()).forEach { (button) in
            button.isExclusiveTouch = true
        }
        self.view.isExclusiveTouch = true
        UIView.appearance().isExclusiveTouch = true
    }
}

extension BaseViewController {
    @IBAction func action_Back(_ sender: UIButton) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func action_Menu(_ sender: Any) {
        //sideMenuController?.revealMenu()
    }
}

//
//  APIConstant.swift
//  Beach Butler
//
//  Created by Mac on 25/08/20.
//  Copyright © 2020 Gaurav Kive. All rights reserved.
//

import Foundation

extension URL {
    
    static let baseUrl = "https://www.beachbutlerbda.com:3000/api/"// "http://3.137.209.155:3000/api/"
    static let baseUrlImage = "https://www.beachbutlerbda.com:3000/"// "http://3.137.209.155:3000/"
    
    //static let host = baseUrl + "ios/v4/"
    //http://54.166.247.66:3000
    enum Key {
        static let accept = "Accept"
        static let contentType = "Content-Type"
        static let authorization = "Authorization"
    }
    enum Header {
        static let JSON = "application/json"
        static let URLEncoded = "application/x-www-form-urlencoded"
    }
    
    enum Home {
        static let login =  baseUrl+"user/login"
        static let socialLogin =  baseUrl+"user/socialAuth/"

        static let register =  baseUrl+"user/register"
                
        static let forgotpassword =  baseUrl+"user/forgot-password"
        static let resetpassword =  baseUrl+"user/reset-password"

        static let verifyotp =  baseUrl+"user/verify-otp"
        static let resendotp =  baseUrl+"user/forgot-password"
    }
    
    enum CountryList{
        static let countryList = baseUrl + "country/list"
    }
    
    enum Search{
        static let sportsList =  baseUrl+"sport/list"
    }
    
    enum UserProfile {
        static let userProfile = baseUrl+"user/profile"
        static let userProfileUpdate = baseUrl+"user/update"
        static let userProfileImageUpdate = baseUrl+"user/userImage"
    }
}



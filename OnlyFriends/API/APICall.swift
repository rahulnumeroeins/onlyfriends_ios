//
//  APICall.swift
//  Beach Butler
//
//  Created by Mac on 25/08/20.
//  Copyright © 2020 Gaurav Kive. All rights reserved.
//

import UIKit
import Alamofire
import MobileCoreServices

class APICall: NSObject {
    
    private static let AlamofireSessionManager: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 120
        let sessionManager = Alamofire.Session(configuration: configuration)
        return sessionManager
    }()
    class func postRequest(url:String,headerValue:HTTPHeaders = [:] ,params:Parameters? ,completionHandler: @escaping CompletionBlock) -> Void{
        
        var header:HTTPHeaders! = nil
        
        /*let token = .sharedManager.read(type: .authorization) as! String
        if  token != "" {
            header = ["token": token]
        } else {
            header = nil
        }
        */
        //var headers = headerValue
        //let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        print("params:",params ?? "")
        
        AF.request(URL.init(string: url)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            print(response.result)

            switch response.result {

            case .success(_):
                if response.value != nil {
                    completionHandler(response)
                }
                break
            case .failure(let error):
                print(error)
                //Spinner.hide()
                break
            }
        }
    }
    class func postRequestForArrayType(url:String,headerValue:HTTPHeaders = [:] ,params:Data? ,completionHandler: @escaping CompletionBlock) -> Void{
        
        var headers = headerValue
        headers[URL.Key.authorization] = "application/json"
        
        print(headers)
        var request = try! URLRequest.init(url: url, method: .post, headers: headers)
         request.httpBody = params
         AlamofireSessionManager.request(request) .responseJSON {response  in
             //completionHandler(response)
         }
    }
    class func getRequest(url:String,headerValue:HTTPHeaders = [:], params:Parameters?,completionHandler: @escaping CompletionBlock) -> Void{
        
        //let headers = headerValue
        
        var header:HTTPHeaders! = nil
        /*let token = ApplicationPreference.sharedManager.read(type: .authorization) as! String
        if  token != "" {
            header = ["token": token]
        } else {
            header = nil
        }
    */
        AF.request(URL.init(string: url)!, method: .get, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            print(response.result)

            switch response.result {

            case .success(_):
                if response.value != nil
                {
                    completionHandler(response)
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    class func postMultiPartRequest(url:String, fileData : Data?, fileName: String?, name: String?, mimeType: String?, headerValue: HTTPHeaders = [:] ,params:Parameters? ,completionHandler: @escaping CompletionBlock) -> Void {
        
        var header:HTTPHeaders! = nil
        /*let token = SharedPreference.sharedManager.read(type: .authorization) as! String
        if  token != "" {
            header = ["token": token]
        } else {
            header = nil
        }*/
        
        print("params:",params ?? "")
        
        AF.upload(multipartFormData: { multipartFormData in
            if fileData != nil {
                multipartFormData.append(fileData!, withName: name!, fileName: fileName, mimeType: mimeType)
            }
            for (key, value) in params ?? [:] {
                multipartFormData.append(((value as? String)?.data(using: .utf8))!, withName: key)
            }
        }, to: URL.init(string: url)!, headers: header).uploadProgress(closure: { (Progress) in
            //print("Upload Progress: \(Progress.fractionCompleted)")
            DispatchQueue.main.async {
                debugPrint(Progress)
            }
        }).responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(_):
                if response.value != nil {
                    completionHandler(response)
                }
                break
            case .failure(let error):
                print(error)
                completionHandler(response)
                break
            }
        }
    }
}

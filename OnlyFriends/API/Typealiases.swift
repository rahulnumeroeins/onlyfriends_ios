//
//  Typealiases.swift
//  Beach Butler
//
//  Created by Mac on 25/08/20.
//  Copyright © 2020 Gaurav Kive. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

typealias StrAny = [String:Any]
typealias StrAnyOpnl = [String:Any]?
typealias ArrayType = [[String:Any]]
typealias CompletionBlock = (_ dataResponse:AFDataResponse<Any>) -> Void
typealias CompletionBlockUpload = (_ dataResponse:AFDataResponse<Any>?, _ error:String?) -> Void

typealias CompletionVoid = (_ success:Bool, _ error:String?) -> Void
typealias Defaults = UserDefaults
typealias Storyboard = UIStoryboard
typealias TableCell = UITableViewCell
typealias Font = UIFont
typealias Color = UIColor
typealias CollectionCell = UICollectionViewCell
//typealias ResponseStatus = (type: ResponseType, description: String)

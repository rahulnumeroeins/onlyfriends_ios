//
//  CustomTabBarController.swift
//  Scarp App Provider
//
//  Created by NumeroEins on 12/09/20.
//  Copyright © 2020 NumeroEins. All rights reserved.
//

import UIKit

class CustomTabBarViewController: UIViewController {
    
    var selectedIndex: Int = 0
    var previousIndex: Int = 0
    
    var viewControllers = [UIViewController]()
    
    @IBOutlet var buttons:[UIButton]!
    @IBOutlet var bgViews:[UIView]!

    @IBOutlet var tabView:UIView!
    @IBOutlet var stackview:UIStackView!
    @IBOutlet var tabViewHeight:NSLayoutConstraint!

    var footerHeight: CGFloat = 80
    
    static let profile = ProfileVC.instantiate(fromStoryboard: .Profile)
    static let discover = DiscoverVC.instantiate(fromStoryboard: .Discover)
    static let messages = MessagesVC.instantiate(fromStoryboard: .Messages)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewControllers.append(CustomTabBarViewController.profile)
        viewControllers.append(CustomTabBarViewController.discover)
        viewControllers.append(CustomTabBarViewController.messages)
        
//        let statusBarHeight : CGFloat
//        if #available(iOS 13, *) {
//            statusBarHeight = sceneDelegate.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0//BaseApp.appDelegate.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
//        } else {
//            statusBarHeight = UIApplication.shared.statusBarFrame.height
//        }
//        self.footerHeight = UINavigationController().navigationBar.frame.height + statusBarHeight
//        self.tabViewHeight.constant = self.footerHeight
        
        for index in buttons.enumerated() {
            index.element.tintColor = #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1).withAlphaComponent(0.59)
        }
        tabChanged(sender: buttons[selectedIndex])
    }
}


// MARK: - Actions
extension CustomTabBarViewController {
    
    @IBAction func tabChanged(sender:UIButton) {
        previousIndex = selectedIndex
        selectedIndex = sender.tag
        
        bgViews[previousIndex].backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        buttons[previousIndex].tintColor = #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1).withAlphaComponent(0.59)

        let previousVC = viewControllers[previousIndex]
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        
        bgViews[sender.tag].backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.7333333333, blue: 0.5803921569, alpha: 1).withAlphaComponent(0.4)
        buttons[sender.tag].tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

        let vc = viewControllers[selectedIndex]
        vc.view.frame = UIApplication.shared.windows[0].frame
        vc.didMove(toParent: self)
        self.addChild(vc)
        self.view.addSubview(vc.view)
        
        self.view.bringSubviewToFront(tabView)
    }
    
    func hideHeader() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            self.tabView.frame = CGRect(x: self.tabView.frame.origin.x, y: (self.view.frame.height + self.view.safeAreaInsets.bottom + 16), width: self.tabView.frame.width, height: self.footerHeight)
        })
    }
    
    func showHeader() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            self.tabView.frame = CGRect(x: self.tabView.frame.origin.x, y: self.view.frame.height - (self.footerHeight + self.view.safeAreaInsets.bottom + 16), width: self.tabView.frame.width, height: self.footerHeight)
        })
    }
}

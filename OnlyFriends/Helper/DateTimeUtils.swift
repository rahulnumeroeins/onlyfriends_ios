//
//  DateTimeUtils.swift
//
//


import Foundation


class DateTimeUtils {
    
    static let sharedInstance = DateTimeUtils()
    
    static let MILLI_SECOND_IN_HOUR:Double = 1000*60*60 //long
    static let MILLI_SECOND_IN_TWO_HOUR:Double = MILLI_SECOND_IN_HOUR * 2
    static let MILLI_SECOND_IN_DAY:Double = MILLI_SECOND_IN_HOUR*24;
    
    private var DF_TIME_hh_mm_aa:DateFormatter?
    private var DF_TIME_yyyy_MM_dd:DateFormatter?
    private var DF_TIME_dd_MMM_yy:DateFormatter?
    private var DF_TIME_dd_MMMM_yyyy:DateFormatter?
    private var DF_TIME_dd_MMM_yyyy:DateFormatter?
    private var DF_TIME_dd_MMM:DateFormatter?
    private var DF_dd_MM_yyyy:DateFormatter?
    private var DF_DAY_MMM_dd_yyyy:DateFormatter?
    private var DF_MMMM:DateFormatter?
    private var DF_FULL_DAY_DD_MM_YYYY:DateFormatter?
    private var DF_dd_MM_yyyy_TIME:DateFormatter?
    private var DF_dd_MM_yyyy_TIME_12_HOURS:DateFormatter?
    private var DF_dd_MM_TIME_12_HOURS:DateFormatter?
    
    private var DF_MMM_dd_yyyy:DateFormatter?
    private var DF_dd_MMM_yyyy: DateFormatter?
    
    init(){
        loadFormatter()
    }
    
    func loadFormatter() {
        
        DF_TIME_hh_mm_aa = DateFormatter()
        if(DF_TIME_hh_mm_aa != nil){
            DF_TIME_hh_mm_aa?.dateFormat = "hh:mm a"
            DF_TIME_hh_mm_aa?.timeZone = TimeZone.current
            DF_TIME_hh_mm_aa?.locale = Locale.current
        }
        
        
        DF_TIME_dd_MMMM_yyyy = DateFormatter()
        if(DF_TIME_dd_MMMM_yyyy != nil){
            DF_TIME_dd_MMMM_yyyy?.dateFormat = "dd-MMMM-yyyy"
            DF_TIME_dd_MMMM_yyyy?.timeZone = TimeZone.current
            DF_TIME_dd_MMMM_yyyy?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM_yyyy = DateFormatter()
        if(DF_TIME_dd_MMM_yyyy != nil){
            DF_TIME_dd_MMM_yyyy?.dateFormat = "dd-MMM-yyyy"
            DF_TIME_dd_MMM_yyyy?.timeZone = TimeZone.current
            DF_TIME_dd_MMM_yyyy?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM = DateFormatter()
        if(DF_TIME_dd_MMM != nil){
            DF_TIME_dd_MMM?.dateFormat = "dd-MMMM"
            DF_TIME_dd_MMM?.timeZone = TimeZone.current
            DF_TIME_dd_MMM?.locale = Locale.current
        }
        
        DF_TIME_yyyy_MM_dd = DateFormatter()
        if(DF_TIME_yyyy_MM_dd != nil){
            DF_TIME_yyyy_MM_dd?.dateFormat = "yyyy/MM/dd HH:mm:ss z"
            DF_TIME_yyyy_MM_dd?.timeZone = TimeZone.current
            DF_TIME_yyyy_MM_dd?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM_yy = DateFormatter()
        if(DF_TIME_dd_MMM_yy != nil){
            DF_TIME_dd_MMM_yy?.dateFormat = "dd/MMM/yy"
            DF_TIME_dd_MMM_yy?.timeZone = TimeZone.current
            DF_TIME_dd_MMM_yy?.locale = Locale.current
        }
        
        DF_DAY_MMM_dd_yyyy = DateFormatter()
        if(DF_DAY_MMM_dd_yyyy != nil){
            DF_DAY_MMM_dd_yyyy?.dateFormat = "EEE, dd MMM, yyyy"
            DF_DAY_MMM_dd_yyyy?.timeZone = TimeZone.current
            DF_DAY_MMM_dd_yyyy?.locale = Locale.current
        }
        
        DF_MMMM = DateFormatter()
        if(DF_MMMM != nil){
            DF_MMMM?.dateFormat = "MMM,yyyy"
            DF_MMMM?.timeZone = TimeZone.current
            DF_MMMM?.locale = Locale.current
        }
        
        DF_FULL_DAY_DD_MM_YYYY = DateFormatter()
        if(DF_FULL_DAY_DD_MM_YYYY != nil){
            DF_FULL_DAY_DD_MM_YYYY?.dateFormat = "EEEE, dd MMMM, yyyy"
            DF_FULL_DAY_DD_MM_YYYY?.timeZone = TimeZone.current
            DF_FULL_DAY_DD_MM_YYYY?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy = DateFormatter()
        if(DF_dd_MM_yyyy != nil){
            DF_dd_MM_yyyy?.dateFormat = "dd/MM/yyyy"
            DF_dd_MM_yyyy?.timeZone = TimeZone.current
            DF_dd_MM_yyyy?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy_TIME  = DateFormatter()
        if(DF_dd_MM_yyyy_TIME != nil){
            DF_dd_MM_yyyy_TIME?.dateFormat = "dd/MM/yyyy HH:mm:ss z"
            DF_dd_MM_yyyy_TIME?.timeZone = TimeZone.current
            DF_dd_MM_yyyy_TIME?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy_TIME_12_HOURS = DateFormatter()
        if(DF_dd_MM_yyyy_TIME_12_HOURS != nil){
            //DF_dd_MM_yyyy_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm a"
            DF_dd_MM_yyyy_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm"
            DF_dd_MM_yyyy_TIME_12_HOURS?.timeZone = TimeZone.current
            DF_dd_MM_yyyy_TIME_12_HOURS?.locale = Locale.current
        }
        
        DF_dd_MM_TIME_12_HOURS = DateFormatter()
        if(DF_dd_MM_TIME_12_HOURS != nil){
            //DF_dd_MM_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm a"
            DF_dd_MM_TIME_12_HOURS?.dateFormat = "dd-MMM HH:mm"
            DF_dd_MM_TIME_12_HOURS?.timeZone = TimeZone.current
            DF_dd_MM_TIME_12_HOURS?.locale = Locale.current
        }
        
        DF_MMM_dd_yyyy = DateFormatter()
        if(DF_MMM_dd_yyyy != nil){
            DF_MMM_dd_yyyy?.dateFormat = "MMM dd, yyyy"
            DF_MMM_dd_yyyy?.timeZone = TimeZone.current
            DF_MMM_dd_yyyy?.locale = Locale.current
        }
        
        DF_dd_MMM_yyyy = DateFormatter()
        if(DF_dd_MMM_yyyy != nil){
            DF_dd_MMM_yyyy?.dateFormat = "dd, MMM yyyy"
            DF_dd_MMM_yyyy?.timeZone = TimeZone.current
            DF_dd_MMM_yyyy?.locale = Locale.current
        }
    }
    
    func currentDateForConfirRX(date:Date) ->String? {
        return (DF_MMM_dd_yyyy?.string(from: date))!
    }
    
    func formatSimpleTime(date:Date) ->String? {
        return (DF_TIME_hh_mm_aa?.string(from: date))!
    }
    
    func formatFancyDate(date:Date) ->String? {
        return (DF_TIME_dd_MMMM_yyyy?.string(from: date))!
    }
    
    func formatFancyShortDate(date:Date) ->String? {
        return (DF_TIME_dd_MMM_yyyy?.string(from: date))!
    }
    
    func formatFancyDateForHistory(date:Date) ->String? {
        return (DF_TIME_dd_MMM?.string(from: date))!
    }
    func  formatSimpleDate(date:Date) -> String?{
        return (DF_TIME_dd_MMM_yy?.string(from: date))!
    }
    
    func formatDateToString(date:Date) -> String{
        return (DF_TIME_yyyy_MM_dd?.string(from: date))!
    }
    
    func formateDateToMonthString(date:Date) -> String{
        return (DF_MMMM?.string(from: date))!
    }
    
    func formateFullDateStr(date:Date) -> String{
        return (DF_FULL_DAY_DD_MM_YYYY?.string(from:date))!
    }
    
    func formateDateTimeToString(date:Date) -> String{
        return (DF_dd_MM_yyyy_TIME?.string(from:date))!
    }
    
    func formateDateTimeTo12HourTimeString(date:Date) -> String{
        return (DF_dd_MM_yyyy_TIME_12_HOURS?.string(from:date))!
    }
    
    func formateShortDateTimeTo12HourTimeString(date:Date) -> String{
        return (DF_dd_MM_TIME_12_HOURS?.string(from: date))!
    }
    
    func formateDateToStringDDMMMYYY(date:Date) -> String{
        return (DF_dd_MMM_yyyy?.string(from:date))!
    }
    
    func formateDateToStringDDMMYYYY(date:Date) -> String{
        return (DF_dd_MM_yyyy?.string(from:date))!
    }
    
    func changeServerTimeStampToDeviceCurrentDateTime(_ dateUnixFormate:Double) -> String?{
        
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd/MM/yyyy , hh:mm a"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    func dateFromServerStr(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss z"
        let convertedStr = dateStr + " 00:00:00 z"
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    
    func dateForCustomLog(dateStr:String, timeStr:String) -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        let convertedStr = dateStr + " " + timeStr
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    
    func formatDayDateFormat(date:Date) -> String?{
        return (DF_DAY_MMM_dd_yyyy?.string(from: date))!
    }
    
    func getDeviceTimeZoneDateStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd MMM yyyy"//"yyyy-MM-dd" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func ChangeTimeStampToDate(unixtimeInterval : Double) -> String? {
        
        let date = Date(timeIntervalSince1970: unixtimeInterval)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MMM dd,yyyy hh:mm a"
        let strDate = dateFormatter.string(from: date)
        // let dtTime = dateFormatter.date(from: strDate)
        return strDate
    }
    
    func timeStampToDate(timeVal:String, dateFormat: String)->String {
        let timeResult:Double =  NumberFormatter().number(from: timeVal)?.doubleValue ?? 0
        let time = timeVal.count > 10 ? timeResult/1000 : timeResult
        let date = Date(timeIntervalSince1970: time)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current//TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = dateFormat//"dd, MMM yyyy hh:mm a"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    func getHrsMinFromDateToCurrentDate(_ dateUnixFormate:Double) -> (Int, Int){
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let serverTimeSeconds = Int(0011)
        
        let startTimeStamp = Int(date.timeIntervalSince1970)*1000
        let endTimeStamp = Int(((Date().timeIntervalSince1970)*1000)) + (serverTimeSeconds)
        let totalSeconds = (endTimeStamp - startTimeStamp)/1000
        
        let hrs = (totalSeconds/60)/60
        let mins: Int = (Int(round(Double(totalSeconds))) / 60) % 60
        
        return (Int(hrs), Int(mins))
    }
    
    //    func dateFromServerHMS_Date(dateStr:String) -> String? {
    //        let dateFormatter = DateFormatter()
    //        dateFormatter.dateStyle = DateFormatter.Style.long
    //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //        let convertedDate = dateFormatter.date(from:dateStr)
    //        let strDate = dateFormatter.string(from: convertedDate!)
    //
    //        return strDate
    //    }
    
    func getDeviceTimeZoneTimeStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "hh:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func getDeviceTimeZoneDateTimeStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MMM dd,yyyy hh:mm a"//"dd/MM/yyyy, hh:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func getCurrentMonthName() -> String{
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let nameOfMonth = dateFormatter.string(from: now)
        return nameOfMonth
    }
    
    func getCurrentDate() -> Date {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM YYYY"
        let strDate = dateFormatter.string(from: now)
        let dt = dateFormatter.date(from: strDate)?.toLocalTime()
        return dt!
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60)
    }
    
    func getDateFromStr(dateStr:String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MMM-dd-yyyy"//"dd/MM/yyyy"
        let convertedDate = dateFormatter.date(from:dateStr)
        dateFormatter.dateFormat = "dd/MM/yyyy" // added
        return dateFormatter.string(from: convertedDate!)
    }
    
    func dateFromStr(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss z"
        //let convertedStr = dateStr + " 00:00:00 z"
        let convertedStr = dateStr
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    
    func dateFromStrWithFormatter(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSZ"
        let convertedDate = dateFormatter.date(from:dateStr)
        return convertedDate
    }
    
    func dateFromString(dateStr:String,dateFormatter:DateFormatter) -> Date? {
        let convertedStr = dateStr + " 00:00:00 z"
        // let convertedStr = dateStr
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    func fancyDateFromStr(dateStr:String) -> Date? {
        let convertedStr = dateStr
        let convertedDate = DF_TIME_dd_MMMM_yyyy?.date(from:convertedStr)
        return convertedDate
    }
    
    func fancyShortDateFromStr(dateStr:String) -> Date? {
        let convertedStr = dateStr
        let convertedDate = DF_TIME_dd_MMM_yyyy?.date(from:convertedStr)
        return convertedDate
    }
    
    func todayDateWithStartTime() -> Date?{
        let todayDate = Date()
        let todayDateStr = DF_dd_MM_yyyy?.string(from: todayDate)
        let todayDateWithTimeStr = "\(todayDateStr!) 00:00:00 z"
        return dateFromStr(dateStr: todayDateWithTimeStr)
    }
    
    func todayDateWithEndTime() -> Date?{
        let todayDate = Date()
        let todayDateStr = DF_dd_MM_yyyy?.string(from: todayDate)
        let todayDateWithTimeStr = "\(todayDateStr!) 23:59:59 z"
        
        return dateFromStr(dateStr: todayDateWithTimeStr)
    }
    
    func getDateFromTimeInterval(timeInterval:TimeInterval) -> Date{
        return Date(timeIntervalSince1970: timeInterval )/// 1000.0)
    }
    
    func getTimeIntervalFromDate(date:Date) -> TimeInterval{
        return (date.timeIntervalSince1970)//*1000
    }
    
    func getimeIntervalStringFromDate(date:Date) -> String{
        return String(describing: Date.timeIntervalSince(date))
    }
    
    func getDateFromStringTimeInterval(timeInterval:String) -> Date?{
        let dateTimeMilliSecondString:String? = BaseApp.sharedInstance.removeOptionalString(timeInterval)
        if(dateTimeMilliSecondString != nil){
            let messageDate:Date? = self.getDateFromTimeInterval(timeInterval: Double(dateTimeMilliSecondString!)!)
            return messageDate
        }
        return nil
    }
    
    func localToUTC(date:Date, fromFormat: String, toFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        //        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = toFormat
        
        return dateFormatter.string(from: date)
    }
    
    func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        
        return dateFormatter.string(from: dt!)
    }
    
    func getDateFromString(strDate:String,strDateFormatter:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = strDateFormatter
        let convertedDate = dateFormatter.date(from:strDate)
        return convertedDate
    }
    
    func getTimeFromServerDate(_ dateUnixFormate:Double) -> Date?{
        
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let strDate = dateFormatter.string(from: date)
        let dtTime = dateFormatter.date(from: strDate)
        return dtTime
    }
    
    func findDateDiff(time1Str: String, time2Str: String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "hh:mma"
        
        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }
        
        //You can directly use from here if you have two dates
        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let intervalInt = Int(interval)
        
        if hour > 0 && minute > 0 {
            return "\(Int(hour))hrs \(Int(minute))min"
        } else if hour > 0 && minute <= 0 {
            return "\(Int(hour))hrs"
        } else if hour <= 0 && minute > 0 {
            return "\(Int(hour))hrs \(Int(minute))min"
        } else {
            return "\(intervalInt < 0 ? "-" : "+") \(Int(hour)) hrs \(Int(minute)) min"
        }
    }
    
    func conevrtServerDateToLocal(_ serverDate: String, dateFormat: String) -> String {
        if serverDate != "" {
            let iso8601DateFormatter = ISO8601DateFormatter()
            iso8601DateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
            let convertedDate = iso8601DateFormatter.date(from: serverDate)
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = dateFormat//"dd MMM yyyy"
            let strDate = dateFormatter.string(from: convertedDate!)
            return strDate
        }
        return serverDate
    }
    
    func getDayDateMonthYear(date: Date, strFormat: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss 'UTC'"
        formatter.dateFormat = strFormat
        let dateString = formatter.string(from: date)
        return dateString
    }
}


extension Date {
    static func getDates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
    
    /// Returns a Date with the specified days added to the one it is called with
    func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        var targetDay: Date
        targetDay = Calendar.current.date(byAdding: .year, value: years, to: self)!
        targetDay = Calendar.current.date(byAdding: .month, value: months, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .day, value: days, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .hour, value: hours, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .minute, value: minutes, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .second, value: seconds, to: targetDay)!
        return targetDay
    }
    
    /// Returns a Date with the specified days subtracted from the one it is called with
    func subtract(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        let inverseYears = -1 * years
        let inverseMonths = -1 * months
        let inverseDays = -1 * days
        let inverseHours = -1 * hours
        let inverseMinutes = -1 * minutes
        let inverseSeconds = -1 * seconds
        return add(years: inverseYears, months: inverseMonths, days: inverseDays, hours: inverseHours, minutes: inverseMinutes, seconds: inverseSeconds)
    }
    
    // MARK: - Format dates
    
    func stringFromFormat(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func stringFromDateFormatter(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.calendar = NSCalendar.current
        formatter.timeZone = TimeZone.current
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.locale = Locale.current
        return formatter.string(from: self)
    }
    
    func differenceWith(_ date: Date, inUnit unit: NSCalendar.Unit) -> Int {
        return (Calendar.current.dateComponents(Set([.year]), from: self, to: Date()) as NSDateComponents).value(forComponent: unit)
    }
    
    //TODO:- "yyyy-MM-DD HH:mm:ss"
    func onlyDate()->Date? {
        //TODO:- "2020-06-10 09:46:49 +0000"
        let dateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        //        let date_ = dateFormatter.string(from: self)
        //
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        let strNewDate = dateFormatter.string(from: self)//?.toLocalTime()
        let newDate = dateFormatter.date(from: strNewDate)
        return newDate
    }
    
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func adding(days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    
    func adding(months: Int) -> Date {
        return Calendar.current.date(byAdding: .month, value: months, to: self)!
    }
    
    func adding(years: Int) -> Date {
        return Calendar.current.date(byAdding: .year, value: years, to: self)!
    }
    
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}



//==================================================
// MARK:- Common methods for SpeakAMe
//==================================================
extension DateTimeUtils {
    
    func messageDataAndTime(){
        //23 may, 11:12AM
        
        
    }
    
    func getUserLastActivityTime(sourceDate : Date) -> String {
        
        
        let formatter: NumberFormatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        
        let currentDate = DateTimeUtils.sharedInstance.getCurrentDate()
        var secondsAgo = Double(currentDate.timeIntervalSince(sourceDate))
        if secondsAgo < 0 {
            secondsAgo = secondsAgo * (-1)
        }
        
        let minute = 60.0
        let hour = 60.0 * minute
        let day = 24 * hour
        let week = 7 * day
        
        if secondsAgo < minute  {
            if secondsAgo < 2 {
                return "just now"
            }else{
                //                return "\(secondsAgo) secs ago"
                return "just now"
            }
        } else if secondsAgo < hour {
            let min = Double(secondsAgo/minute)
            
            let roundedMin = round(min)
            let roundedMinInt = Int(roundedMin)
            
            let strMin = roundedMinInt.toString()//formatter.string(from: (min) as NSNumber)
            if roundedMinInt == 1{
                return "\(strMin ?? "") min ago"
            }else{
                return "\(strMin ?? "") min ago"
            }
        } else if secondsAgo < day {
            let hr = Double(secondsAgo/hour)
            
            let roundedHr = round(hr)
            let roundedHrInt = Int(roundedHr)
            
            let strHr = roundedHrInt.toString()//formatter.string(from: (hr) as NSNumber)
            if roundedHrInt == 1{
                return "\(strHr ?? "") hr ago"
            } else {
                return "\(strHr ?? "") hrs ago"
            }
        } else if secondsAgo < week {
            let days = Double(secondsAgo/day)
            let rounded = Double(round(days))
            let roundedDays = Int(rounded)
            //let strDays = formatter.string(from: (days) as NSNumber)
            if roundedDays == 1{
                return "Yesterday"//"\(day) day ago"
            }else{
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                formatter.locale = Locale(identifier: "en_US")
                let strDate: String = formatter.string(from: sourceDate)
                return strDate
            }
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            formatter.locale = Locale(identifier: "en_US")
            let strDate: String = formatter.string(from: sourceDate)
            return strDate
        }
    }
}

extension Date {
    /*
     func getChatMessageHeader() -> String {
     
     
     let sourceDate = self.toLocalTime()
     
     let currentDate = Date().toLocalTime()//DateTimeUtils.sharedInstance.getCurrentDate()
     var secondsAgo = Double(currentDate.timeIntervalSince(sourceDate))
     if secondsAgo < 0 {
     secondsAgo = secondsAgo * (-1)
     }
     
     let minute = 60.0
     let hour = 60.0 * minute
     let day = 24 * hour
     let week = 7 * day
     
     if secondsAgo < minute  {
     //            if secondsAgo < 2 {
     //                return "just now"
     //            }else{
     //                //                return "\(secondsAgo) secs ago"
     //                return "just now"
     //            }
     return "Today"
     } else if secondsAgo < hour {
     //            let min = Double(secondsAgo/minute)
     //
     //            let roundedMin = round(min)
     //            let roundedMinInt = Int(roundedMin)
     //
     //            let strMin = roundedMinInt.toString()//formatter.string(from: (min) as NSNumber)
     //            if roundedMinInt == 1{
     //                return "\(strMin ?? "") min ago"
     //            }else{
     //                return "\(strMin ?? "") min ago"
     //            }
     return "Today"
     } else if secondsAgo < day {
     return "Today"
     //            let hr = Double(secondsAgo/hour)
     //
     //            let roundedHr = round(hr)
     //            let roundedHrInt = Int(roundedHr)
     //
     //            let strHr = roundedHrInt.toString()//formatter.string(from: (hr) as NSNumber)
     //            if roundedHrInt == 1{
     //                return "\(strHr ?? "") hr ago"
     //            } else {
     //                return "\(strHr ?? "") hrs ago"
     //            }
     } else if secondsAgo < week {
     let days = Double(secondsAgo/day)
     let rounded = Double(round(days))
     let roundedDays = Int(rounded)
     //let strDays = formatter.string(from: (days) as NSNumber)
     if roundedDays == 1{
     return "Yesterday"//"\(day) day ago"
     }else{
     let formatter = DateFormatter()
     formatter.dateFormat = "dd MMM yyyy"
     formatter.locale = Locale(identifier: "en_US")
     let strDate: String = formatter.string(from: sourceDate)
     return strDate
     }
     } else {
     let formatter = DateFormatter()
     formatter.dateFormat = "dd MMM yyyy"
     formatter.locale = Locale(identifier: "en_US")
     let strDate: String = formatter.string(from: sourceDate)
     return strDate
     }
     }
     
     func getChatListTime()->String
     {
     
     let sourceDate = self.toLocalTime()
     
     let currentDate = Date().toLocalTime()
     var secondsAgo = Double(currentDate.timeIntervalSince(sourceDate))
     if secondsAgo < 0 {
     secondsAgo = secondsAgo * (-1)
     }
     
     let minute = 60.0
     let hour = 60.0 * minute
     let day = 24 * hour
     let week = 7 * day
     
     if secondsAgo < minute  {
     if secondsAgo < 2 {
     return "just now"
     }else{
     //                return "\(secondsAgo) secs ago"
     return "just now"
     }
     //            return "Today"
     } else if secondsAgo < hour {
     let min = Double(secondsAgo/minute)
     
     let roundedMin = round(min)
     let roundedMinInt = Int(roundedMin)
     
     let strMin = roundedMinInt.toString()//formatter.string(from: (min) as NSNumber)
     if roundedMinInt == 1{
     return "\(strMin ?? "") min ago"
     }else{
     return "\(strMin ?? "") min ago"
     }
     //            return "Today"
     } else if secondsAgo < day {
     //            return "Today"
     let hr = Double(secondsAgo/hour)
     
     let roundedHr = round(hr)
     let roundedHrInt = Int(roundedHr)
     
     let strHr = roundedHrInt.toString()//formatter.string(from: (hr) as NSNumber)
     if roundedHrInt == 1{
     return "\(strHr ?? "") hr ago"
     } else {
     return "\(strHr ?? "") hrs ago"
     }
     } else if secondsAgo < week {
     let days = Double(secondsAgo/day)
     let rounded = Double(round(days))
     let roundedDays = Int(rounded)
     //let strDays = formatter.string(from: (days) as NSNumber)
     if roundedDays == 1{
     return "Yesterday"//"\(day) day ago"
     }else{
     let formatter = DateFormatter()
     formatter.dateFormat = "dd MMM yyyy"
     formatter.locale = Locale(identifier: "en_US")
     let strDate: String = formatter.string(from: sourceDate)
     return strDate
     }
     } else {
     let formatter = DateFormatter()
     formatter.dateFormat = "dd MMM yyyy"
     formatter.locale = Locale(identifier: "en_US")
     let strDate: String = formatter.string(from: sourceDate)
     return strDate
     }
     }
     */
    func timeAgoSinceDate(numericDates:Bool = false) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date().toLocalTime()
        let earliest = now < self ? now : self
        let latest = (earliest == now) ? self : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
                //                latest
                
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            //            return "\(components.hour!) hours ago"
            return "Today"
        } else if (components.hour! >= 1){
            if (numericDates){
                //                return "1 hour ago"
                return "Today"
            } else {
                //                return "An hour ago"
                return "Today"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
            //            return "Today"
        } else if (components.minute! >= 1){
            if (numericDates){
                //                return "1 minute ago"
                return "Today"
            } else {
                //                return "A minute ago"
                return "Today"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
            //            return "Today"
        } else {
            return "Just now"
            //            return "Today"
        }
        
    }
    
    func seenAtAgo(numericDates:Bool = false) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date().toLocalTime()
        let earliest = now < self ? now : self
        let latest = (earliest == now) ? self : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
                
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
            
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
                
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
            
        } else {
            return "Just now"
        }
        
    }
    
    func getTimeOnlyForMessages() -> String? {
        
        //            let doubleTimeInterval = timeInterval.toNSNumber()?.doubleValue
        //            let timeinterval = TimeInterval(exactly: doubleTimeInterval!)
        //            var date_ = Date(timeIntervalSince1970: timeinterval!)
        
        // create dateFormatter with UTC time format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.date(from: self.stringFromFormat("yyyy-MM-dd HH:mm:ss"))
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        let time = dateFormatter.string(from: date!)
        return time
    }
    
    func toLocalTime() -> Date {
        let timeZone = NSTimeZone.local
        let seconds : TimeInterval = Double(timeZone.secondsFromGMT(for:self))
        let localDate = Date.init(timeInterval: seconds, since: self)
        return localDate
    }
}

//
//  UITextField+Extensions.swift
//  TopTenDoctors
//
//  Created by Honey on 11/05/20.
//  Copyright © 2020 NumeroEins. All rights reserved.
//

import UIKit


extension UITextField {
    
    func setLeftPadding( _ leftMargin : Int ) {
        let vwPadding : UIView = UIView(frame: CGRect(x: 0, y:0, width: leftMargin, height: Int(self.frame.size.height) ) )
        self.leftView = vwPadding
        self.leftViewMode = UITextField.ViewMode.always
    }
    
    func setRightPadding(_ amount: Int) {
           let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: Int(self.frame.size.height)))
           self.rightView = paddingView
           self.rightViewMode = .always
    }
    
    func setImageOnleftView(img : String ,leftPading : Bool, isBottomBorder : Bool ) {
        
        if !img.isEmpty && leftPading {
            
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 50, height: 40) )
            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 20, y: 13, width: 15, height: 15))
            imgVw.image = UIImage(named: img)
            vwForImg.addSubview(imgVw)
            self.leftView = vwForImg
            self.leftViewMode = UITextField.ViewMode.always
            
        } else if !img.isEmpty {
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 30, height: 35) )
            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 5, y: 10, width: 15, height: 15))
            imgVw.image = UIImage(named: img)
            vwForImg.addSubview(imgVw)
            self.leftView = vwForImg
            self.leftViewMode = UITextField.ViewMode.always
            
        }  else if leftPading {
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 10, height: 35) )
            self.leftView = vwForImg
            self.leftViewMode = UITextField.ViewMode.always
        }
        
        if isBottomBorder {
            
            let bottomLayerEmail = CALayer()
            bottomLayerEmail.frame = CGRect(x: 0, y: self.frame.height-1, width: 1000, height: 0.6)
            bottomLayerEmail.backgroundColor = #colorLiteral(red: 0.5704585314, green: 0.5704723597, blue: 0.5704649091, alpha: 1)
            self.layer.addSublayer(bottomLayerEmail)
            self.clipsToBounds = true
        }
    }
    
    func setImageOnRightView(img : String ,rightPadding : Bool, isBottomBorder : Bool ) {
        
        
        if !img.isEmpty && rightPadding {
            
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 20, height: 35) )
            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 6, y: 13, width: 8, height: 8))
            imgVw.image = UIImage(named: img)
            //imgVw.contentMode = .scaleAspectFit
            vwForImg.addSubview(imgVw)
            self.rightView = vwForImg
            self.rightViewMode = UITextField.ViewMode.always
            
        } else if  !img.isEmpty {
            
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 15, height: 35) )
            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 0, y: 10, width: 15, height: 15))
            imgVw.image = UIImage(named: img)
            vwForImg.addSubview(imgVw)
            self.rightView = vwForImg
            self.rightViewMode = UITextField.ViewMode.always
            
        }  else if rightPadding {
            let vwLeft : UIView = UIView(frame: CGRect(x: 0, y:0, width: 10, height: 35) )
            self.rightView = vwLeft
            self.rightViewMode = UITextField.ViewMode.always
        }
        
        if isBottomBorder {
            let bottomLayerEmail = CALayer()
            bottomLayerEmail.frame = CGRect(x: 0, y: self.frame.height-1, width: 1000, height: 0.6)
            bottomLayerEmail.backgroundColor = UIColor.darkGray.cgColor
            self.layer.addSublayer(bottomLayerEmail)
            self.clipsToBounds = true
        }
    }
    func setImageOnPassword(imgActive : String ,imgInActive : String) {
        if  !imgActive.isEmpty {
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 35, height: 50) )
            var eyeButton : UIButton = UIButton(type: .custom)
            eyeButton  = UIButton(frame: CGRect(x: 0, y: 25, width: 23, height: 18))
            eyeButton.setImage(UIImage(named: imgInActive), for: .normal)
            eyeButton.setImage(UIImage(named: imgActive), for: .selected)
            vwForImg.addSubview(eyeButton)
            eyeButton.addTarget(self, action: #selector(eyeButtonAction(sender:)), for: .touchUpInside)
            self.rightView = vwForImg
            self.rightViewMode = UITextField.ViewMode.always
        }
    }
    
    @objc func eyeButtonAction(sender:UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            self.isSecureTextEntry = true
        }else{
            sender.isSelected = true
            self.isSecureTextEntry = false
        }
    }
    
    func setImagebothSide(imgs : [UIImage]) {
        
        let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 25, height: 35) )
        let imgVw : UIImageView = UIImageView(frame: CGRect(x: 0, y: 10, width: 15, height: 15))
        imgVw.image = imgs[0]
        vwForImg.addSubview(imgVw)
        self.leftView = vwForImg
        self.leftViewMode = UITextField.ViewMode.always
        
        
        let vwForImgR : UIView = UIView(frame: CGRect(x: 0, y:0, width: 35, height: 35) )
        let imgVwR : UIImageView = UIImageView(frame: CGRect(x: 0, y: 10, width: 15, height: 15))
        imgVwR.image = imgs[1]
        vwForImgR.addSubview(imgVwR)
        self.rightView = vwForImgR
        self.rightViewMode = UITextField.ViewMode.always
        
        
    }
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
//        self.layer.shadowColor = UIColor.seperatorGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 0.0
    }
    
    func setInputViewDatePicker(target: Any, selector: Selector) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }

}


class CustomTextView:UITextView{
    
    /// A UIImage value that set LeftImage to the UITextView
    @IBInspectable open var leftImage:UIImage? {
        didSet {
            if (leftImage != nil) {
                self.applyLeftImage(leftImage!)
            }
        }
    }


fileprivate func applyLeftImage(_ image: UIImage) {
        let icn : UIImage = image
        let imageView = UIImageView(image: icn)
        imageView.frame = CGRect(x: 0, y: 2.0, width: icn.size.width + 20, height: icn.size.height)
    imageView.contentMode = UIView.ContentMode.center
        //Where self = UItextView
        self.addSubview(imageView)
        self.textContainerInset = UIEdgeInsets(top: 2.0, left: icn.size.width + 10.0 , bottom: 2.0, right: 2.0)
    }
}


extension Int {
    func formatnumber() -> String {
        let formater = NumberFormatter()
        formater.groupingSeparator = ","
        formater.numberStyle = .decimal
        return formater.string(from: NSNumber(value: self))!
    }
}

extension String {
    func formatnumber() -> String {
        let formater = NumberFormatter()
        formater.groupingSeparator = ","
        formater.numberStyle = .decimal
        let intvalue = Int(self)!
        return formater.string(from: NSNumber(value:intvalue))!
    }
}

//
//  ApplicationPreference.swift
//  Scarp App
//
//  Created by Admin on 28/10/20.
//  Copyright © 2020 Gaurav Kive. All rights reserved.
//

import Foundation
import UIKit

protocol ApplicationPreferenceHandler {
    func read(type:PreferenceType) -> Any
    func write(type:PreferenceType,value: Any)
    func clearData(type:PreferenceType)
}

enum PreferenceType:String {
    case appLaunchFirstTime
    case isGuestUser
    case logindata
    case authorization
    case appLangKey
    case deviceId
    case mobile_verify
    case notification_unread_count
    case userLat
    case userLong
    case userAddress
    case fcmToken
    case showTutorial
    case user_Id
    case userType
    case isPlanPurchased
    case socialLogin
    case user_name

    var description : String {
        switch self {
        case .appLaunchFirstTime: return "appLaunchFirstTime"
        case .isGuestUser : return "isGuestUser"
        case .logindata: return "logindata"
        case .authorization: return "authorization"
        case .appLangKey : return "AppLangKey"
        case .deviceId : return "device_id"
        case .mobile_verify : return "mobile_verify"
        case .notification_unread_count : return "notification_unread_count"
        case .userLat : return "userLat"
        case .userLong : return "userLong"
        case .userAddress : return "userAddress"
        case .fcmToken: return "fcmToken"
        case .showTutorial: return "showTutorial"
        case .user_Id: return "user_Id"
        case .userType: return "userType"
        case .isPlanPurchased: return "isPlanPurchased"
        case .socialLogin: return "socialLogin"
        case .user_name: return "user_name"
        }
    }
}

class ApplicationPreference{
    fileprivate static let userDefault = UserDefaults.standard
    
    static var sharedManager: ApplicationPreference {
        return ApplicationPreference()
    }
}

extension ApplicationPreference:ApplicationPreferenceHandler{
    
    func clearData(type: PreferenceType) {
        ApplicationPreference.userDefault.removeObject(forKey: type.description)
        ApplicationPreference.userDefault.synchronize()
    }
    
    func write(type: PreferenceType, value: Any) {
        ApplicationPreference.userDefault.set(value, forKey: type.description)
        ApplicationPreference.userDefault.synchronize()
    }
    
    func read(type: PreferenceType) -> Any {
        return ApplicationPreference.userDefault.object(forKey: type.description) ?? ""
    }
    
    func clearDataOnLogout () {
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.isGuestUser.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.logindata.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.authorization.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.mobile_verify.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.user_Id.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.userType.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.isPlanPurchased.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.socialLogin.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.user_name.description)

        ApplicationPreference.userDefault.synchronize()
        
        UserDefaults.standard.removeObject(forKey: "UserData")
        UserDefaults.standard.synchronize()
    }
    
    func clearAllData () {
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.appLaunchFirstTime.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.isGuestUser.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.logindata.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.authorization.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.mobile_verify.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.user_Id.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.userType.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.isPlanPurchased.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.socialLogin.description)
        ApplicationPreference.userDefault.removeObject(forKey: PreferenceType.user_name.description)

        UserDefaults.standard.removeObject(forKey: "UserData")
        UserDefaults.standard.synchronize()
        
        ApplicationPreference.userDefault.synchronize()
    }
}

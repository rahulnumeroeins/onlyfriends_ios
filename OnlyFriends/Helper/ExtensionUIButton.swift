//
//  ExtensionUIButton.swift
//
//

import Foundation
import UIKit
let imageCache = NSCache<NSString, AnyObject>()

//MARK:- UIButton Extension to set image icon at any position
extension UIButton {
    
    //    This method sets an image and title for a UIButton and
    //    repositions the titlePosition with respect to the button image.
    //    Add additionalSpacing between the button image & title as required
    //    For titlePosition, the function only respects UIViewContentModeTop, UIViewContentModeBottom, UIViewContentModeLeft and UIViewContentModeRight
    //    All other titlePositions are ignored
    @objc func set(image anImage: UIImage?, title: NSString!, titlePosition: UIView.ContentMode, additionalSpacing: CGFloat, state: UIControl.State){
        self.imageView?.contentMode = .center
        self.setImage(anImage, for: state)
        
        positionLabelRespectToImage(title: title!, position: titlePosition, spacing: additionalSpacing)
        
        self.titleLabel?.contentMode = .center
        self.setTitle(title as String?, for: state)
    }
    
    private func positionLabelRespectToImage(title: NSString, position: UIView.ContentMode, spacing: CGFloat) {
        let imageSize = self.imageRect(forContentRect: self.frame)
        let titleFont = self.titleLabel?.font!
        let titleSize = title.size(withAttributes: [NSAttributedString.Key.font: titleFont!])
        
        var titleInsets: UIEdgeInsets
        var imageInsets: UIEdgeInsets
        
        switch (position){
        case .top:
            titleInsets = UIEdgeInsets(top: -(imageSize.height + titleSize.height + spacing), left: -(imageSize.width), bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleSize.width)
        case .bottom:
            titleInsets = UIEdgeInsets(top: (imageSize.height + titleSize.height + spacing), left: -(imageSize.width), bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleSize.width)
        case .left:
            titleInsets = UIEdgeInsets(top: 0, left: -(imageSize.width * 2), bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -(titleSize.width * 2 + spacing))
        case .right:
            titleInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -spacing)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        default:
            titleInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        self.titleEdgeInsets = titleInsets
        self.imageEdgeInsets = imageInsets
    }
    
    func playImplicitBounceAnimation() {
        
        let bounceAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        bounceAnimation.values = [1.0 ,1.4, 0.9, 1.15, 0.95, 1.02, 1.0]
        bounceAnimation.duration = TimeInterval(0.5)
        bounceAnimation.calculationMode = CAAnimationCalculationMode.cubic
        
        layer.add(bounceAnimation, forKey: "bounceAnimation")
    }
    
    func playExplicitBounceAnimation() {
        
        let bounceAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        var values = [Double]()
        let e = 2.71
        
        for t in 1..<100 {
            let value = 0.6 * pow(e, -0.045 * Double(t)) * cos(0.1 * Double(t)) + 1.0
            
            values.append(value)
        }
        
        bounceAnimation.values = values
        bounceAnimation.duration = TimeInterval(0.5)
        bounceAnimation.calculationMode = CAAnimationCalculationMode.cubic
        
        layer.add(bounceAnimation, forKey: "bounceAnimation")
    }
    
    func setImageColor(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }
    
    func setImageColorForState(image: UIImage, color: UIColor, forState: UIControl.State) {
        let temp = image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        setImage(temp, for: forState)
        tintColor = color
    }
    
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(colorImage, for: forState)
    }
    
    //FOR UNDERLINE OF BUTTON :- NIRANJAN 
    func setTitleWithUnderline(_ title: String?, for state: UIControl.State,with color:UIColor) {
        self.setTitle(title, for: .normal)
        self.setAttributedTitle(self.attributedString(withColor: color), for: .normal)
    }
    
    //FOR UNDERLINE OF BUTTON WITHOUT COLOR:- NIRANJAN
    func setTitleWithUnderline(_ title: String?, for state: UIControl.State) {
        self.setTitle(title, for: .normal)
        self.setAttributedTitle(self.attributedString(withColor: UIColor.gray), for: .normal)
        
    }
    
    
    private func attributedString(withColor:UIColor) -> NSAttributedString? {
        let attributes : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.init(name: "Lato-Regular", size: 16) ?? UIFont.systemFont(ofSize: 16),
            NSAttributedString.Key.foregroundColor : withColor,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
        ]
        let attributedString = NSAttributedString(string: self.currentTitle!, attributes: attributes)
        return attributedString
    }
    
    //
    public func setImageFromURl(_ urlString: String) {
        
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        let size = self.frame.size;
        activityIndicator.frame = CGRect.init(x: 0.0, y: 0.0, width: 44, height: 44)
        activityIndicator.color = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        activityIndicator.startAnimating()
        activityIndicator.center = CGPoint(x: (size.width/2), y: (size.height/2));
        self.addSubview(activityIndicator) // add spinner
        // check cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString) as? UIImage {
            
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.fade
            //self.layer.add(transition, forKey: nil)
            //self.imageView?.image = cachedImage
            self.setImage(cachedImage, for: .normal)
            activityIndicator.removeFromSuperview() // removed spinner when cachedImage
            return
        }
        // loading image by URL
        // URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
        let encodeUrl: String? = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        URLSession.shared.dataTask(with: URL(string: encodeUrl!)!, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "No Error")
                activityIndicator.removeFromSuperview() // removed spinner when image loaded
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                activityIndicator.removeFromSuperview() // removed spinner when image loaded
                
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = CATransitionType.fade
                // self.layer.add(transition, forKey: nil)
                
                //self.imageView?.image = image
                if image != nil {
                    self.setImage(image, for: .normal)
                    imageCache.setObject(image!, forKey: urlString as NSString)
                }
            })
        }).resume()
    }
    
    //
    public func setBackgroundImageFromURL(_ urlString: String) {
        
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        let size = self.frame.size;
        activityIndicator.frame = CGRect.init(x: 0.0, y: 0.0, width: 44, height: 44)
        activityIndicator.color = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        activityIndicator.startAnimating()
        activityIndicator.center = CGPoint(x: (size.width/2), y: (size.height/2));
        self.addSubview(activityIndicator) // add spinner
        // check cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString) as? UIImage {
            
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.fade
            //self.layer.add(transition, forKey: nil)
            //self.imageView?.image = cachedImage
            self.setBackgroundImage(cachedImage, for: .normal)
            activityIndicator.removeFromSuperview() // removed spinner when cachedImage
            return
        }
        // loading image by URL
        //URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
        let encodeUrl: String? = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        URLSession.shared.dataTask(with: URL(string: encodeUrl!)!, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "No Error")
                activityIndicator.removeFromSuperview() // removed spinner when image loaded
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                activityIndicator.removeFromSuperview() // removed spinner when image loaded
                
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = CATransitionType.fade
                
                //self.imageView?.image = image
                if image != nil {
                    self.setBackgroundImage(image, for: .normal)
                    imageCache.setObject(image!, forKey: urlString as NSString)
                }
            })
        }).resume()
    }
}

extension UIButton {

    func alignImageAndTitleVertically(padding: CGFloat = 6.0) {
        let imageSize = self.imageView!.frame.size
        let titleSize = self.titleLabel!.frame.size
        let totalHeight = imageSize.height + titleSize.height + padding

        self.imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageSize.height),
            left: 0,
            bottom: 0,
            right: -titleSize.width
        )

        self.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: -imageSize.width,
            bottom: -(totalHeight - titleSize.height),
            right: 0
        )
    }

}


//
//  LocalizationKeys.swift
//

import Foundation

enum LocalizationKeys: String {
    
    //App level related message strings
    case app_name
    case title_app_update
    case msg_new_version
    case title_session_expire
    case title_error
    case title_message
    case title_welcome
    case title_coming_soon
    case titleResetPassword
    case characterCount
    //Internet related message strings
    case title_no_internet
    case msg_no_internet
    
    case title_alert
    case title_failure
    case title_gps_failure
    case msg_location_turned_off
    case title_settings
    
    //
    case title_take_photo_text
    case title_gallery_text
    case msg_empty_profile_pic
    case msg_unable_to_pick_file
    case msg_device_support_issue
    case msg_confrimation_delete_account
    case msg_confrimation_logout
    case msg_issue_email_send
    
    case msg_time_an_hour_ago
    case msg_time_today_at
    case msg_time_yesterday_at
    
    //MARK:Button Titles
    /////////////////////Button Titles//////////////////////
    case btn_skip
    case btn_ok
    case btn_cancel
    case btn_delete
    case btn_save
    case btn_next
    case btn_update
    case btn_submit
    case btn_verify
    case btn_done
    case btn_continue
    case btn_pay
    case btn_decline
    case btn_accept
    case btn_upload
    case btn_yes
    case btn_no
    case btn_proceed
    case btn_t_and_c_text
    case btn_t_and_c
    case btn_signup_now_text
    case btn_signup_now
    case btn_go_home
    case btn_stay_logged
    case btn_register
    case btn_signin
    case btn_signup
    case btn_signup_email
    case btn_login
    case btn_log_in
    case btn_forgot_pwd
    case btn_reset_pwd
    case btn_reset
    case btn_resend_verifciation_code
    case btn_skip_for_now
    case btn_send_feedback
    case btn_invite
    case btn_go_back
    case btn_download_qrcode
    case btn_edit
    case btn_reorder
    case btn_complete_order
    case btn_send
    case btn_male
    case btn_female
    case btn_other
    case btn_set
    case btn_remove_org
    case btn_create_account
    case btn_need_help
    case btn_Login_here
    case btn_agree
    case btn_terms_conditions
    case btn_terms_services
    case btn_privacy_policy
    case btn_continue_with_email
    case btn_save_and_continue
    case btn_confirm

    //MARK:PLACEHOLDERS
    /////////////////////Placeholders//////////////////////
    case placeholder_email_mobile_number
    case placeholder_email
    case placeholder_password
    case placeholder_name
    case placeholder_first_name
    case placeholder_last_name
    case placeholder_full_name
    case placeholder_quote
    case placeholder_affirmation
    case placeholder_phone
    case placeholder_id
    case placeholder_select_country
    case placeholder_state
    case placeholder_city_county
    case placeholder_town
    case placeholder_city
    case placeholder_code
    case placeholder_street_number
    case placeholder_flate_number
    case placeholder_pincode
    case placeholder_address
    case placeholder_address_line_first
    case placeholder_username
    case placeholder_new_password
    case placeholder_confirm_password
    case placeholder_old_password
    case placeholder_current_password
    case placeholder_one_time_password
    case placeholder_phone_number
    case placeholder_dob
    case placeholder_date_of_birth
    case placeholder_verification_code
    case placeholder_write_here
    case placeholder_upload_id
    case placeholder_referral_code
    case placeholder_enter_password
    case placeholder_age
    case placeholder_gender
    case placeholder_id_image
    case placeholder_pin
    case placeholder_confrim_pin
    
    //MARK:VALIDATION
    /////////////////////Validations///////////////////////
    //LOGIN
    case msg_validation_empty_email
    case msg_validation_valid_email
    case msg_validation_empty_password
    case msg_validation_valid_password
    case msg_validation_valid_email_or_mobilenumber
    case msg_email_not_verified
    case disclaimer_detail
    
    //SIGNUP
    case msg_validation_empty_username
    case msg_validation_empty_name
    case msg_validation_empty_firstname
    case msg_validation_empty_lastname
    case msg_validation_empty_fullname
    case msg_validation_empty_country
    case msg_validation_empty_state
    case msg_validation_empty_city_county
    case msg_validation_empty_city
    case msg_validation_empty_street
    case msg_validation_empty_flat_number
    case msg_validation_empty_pincode
    case msg_validation_empty_address
    case msg_validation_select_t_c
    case msg_validation_empty_mobile_number
    case msg_validation_valid_mobile_number
    case msg_validation_empty_country_code
    case msg_validation_empty_one_time_password
    case msg_validation_empty_subject
    case msg_validation_empty_message
    case msg_validation_empty_dob
    case msg_signup_enter_verification_code_otp_description
    case msg_validation_empty_verification_code
    case msg_validation_valid_verification_code
    case msg_validation_upload_id
    case msg_validation_empty_age
    case msg_validation_empty_gender
    case msg_validation_select_profile_picture
    case msg_validation_empty_bio
    
    //Reset Password
    case msg_validation_empty_current_password
    case msg_validation_empty_new_password
    case msg_validation_empty_confirm_password
    case msg_validation_valid_current_password
    case msg_validation_valid_new_passwd
    case msg_validation_pwd_not_match
    case msg_validation_pwd_not_same
    case msg_validation_pin_not_same
    case msg_validation_empty_pin
    case msg_validation_valid_pin
    case msg_validation_country_code
    
    case msg_validation_empty_quote
    case msg_validation_empty_affirmation
    //MARK:Header
    /////////////////////Header//////////////////////
    case title_lets_go
    case title_lets_get_started
    
    
    /**************************************************/
    //MARK:ALL SCREEN TITLES, PLACEHOLDER , VALIDATIONS
    /**************************************************/
    //Tutorial screen
    case title_tutorial_page1
    case desc_tutorial_page1
    case title_tutorial_page2
    case desc_tutorial_page2
    case title_tutorial_page3
    case desc_tutorial_page3
    
    //Login
    case title_login
    case title_login_now_text
    
    //Signup
    case title_signup
    case title_signup_text
    case title_signup_now_text
    case title_signup_and
    case title_signup_set_password
    case title_signup_set_password_txt
    case title_terms_conditions
    case placeholder_refferal_points

    //Verify Email
    case title_thankyou_for_signup_text
    case title_verification_email_text
    case title_resend_verification_code
    case title_enter_here
    
    //Forgot Password
    case title_forgot_password
    case title_forgot_password_text
    
    case title_reset_password
    case title_otp_txt
    
    //Settings
    case title_account
    case title_my_profile
    case title_my_profile_edit
    case title_change_password
    case title_manage_payment
    case title_my_boats
    case title_about_the_app
    case title_privacy_policy
    case title_terms_conditions_settings
    case title_share_the_app
    case title_rate_the_app
    case title_help
    case title_logout
    
    //More Contact us
    case title_call_us
    case title_email_us
    case title_website
    
    //Profile
    case title_novice
    case title_intermediate
    case title_expert
    
    //Dashbaord
    case title_home
    case title_create_trip
    case title_notifications
    case title_total_trips
    case title_total_boats
    case title_view_boats
    case title_ongoing_trips
    case title_upcoming_trips
    case title_pending_trips
    case title_completed_trips
    
    //Add Boat
    case title_boat_details
    case title_add_boat
    case title_edit_boat
    case title_add_default_pic
    case title_add_more_pic
    case placeholder_boat_type
    case placeholder_boat_name
    case placeholder_boat_seats
    case placeholder_boat_bio
    case msg_validation_empty_boat_type
    case msg_validation_empty_boat_name
    case msg_validation_empty_boat_seat
    case msg_validation_valid_boat_seat
    case msg_validation_empty_boat_bio
    case msg_validation_empty_boat_default_pic
    
    //Create Trip
    case msg_validation_select_trip_name
    case msg_validation_select_trip_date
    case msg_validation_select_trip_time
    case msg_validation_select_trip_duration
    case msg_validation_select_valid_trip_duration
    case msg_validation_select_trip_budget
    case msg_validation_select_valid_trip_budget
    case msg_validation_select_boat
    case msg_validation_select_meeting_point
    case msg_validation_select_destination_point
    case msg_validation_select_target_species
    case msg_validation_select_trip_desc
    case title_create_trip_success
    case placeholder_trip_name
    case placeholder_trip_date
    case placeholder_trip_time
    case placeholder_trip_duration
    case placeholder_trip_budget
    case placeholder_trip_select_boat
    case placeholder_trip_meeting_point
    case placeholder_trip_destination_point
    case placeholder_trip_equipment_supplied
    case placeholder_trip_target_species
    case placeholder_trip_desc
    case title_calculate_trip_price
    case placeholder_cost_per_person
    case placeholder_no_of_person
    case title_total_cost
    case btn_create
    case title_hours
    
    //
    case msg_validation_empty_search
    case placeholder_search_trip
    case title_upcoming_trip
    case title_pending_trip
    case title_completed_trip
    
    //Manage Payment
    case btn_make_default
    case title_account_holder
    case title_account_number
    case title_ifsc_number
    case title_add_new_account
    case placeholder_account_number
    case placeholder_account_holder
    case placeholder_ifsc_code
    case btn_add_account
    case msg_validation_empty_acc_number
    case msg_validation_empty_acc_holder_name
    case msg_validation_valid_acc_holder_name
    case msg_validation_empty_ifsc
    case msg_validation_valid_acc_number
    case msg_validation_valid_ifsc_number
    
    //Trip Details
    case title_trip_details
    case title_trip_date
    case title_trip_time
    case title_trip_id
    case title_trip_destination
    case title_trip_meetup_point
    case title_trip_target_species
    case title_tirp_boat
    case title_trip_cost
    case title_trip_duration
    case trip_seats
    case title_about_trip
    case btn_trip_pay
    case btn_trip_cancel
    case btn_trip_start
    case btn_trip_confrim
    case btn_trip_complete
    case btn_trip_join
    case btn_trip_rate_n_review
    
    //
    case title_org_details
    case title_org_bio
    case title_review_on_trip
    case title_review_by
    
    //
    case title_joiners
    case title_joiners_details
    
    //Org Cancel
    case title_comments
    case msg_validation_empty_reson_cancellation
}

extension LocalizationKeys {
    func getLocalized()-> String {
        return self.rawValue.localized()
    }
}

extension String {
    func localized()-> String {
        return BaseApp.sharedInstance.getMessageForCode(self)!
    }
}

extension String {
    var localizedLang: String {
        return NSLocalizedString(self, comment: "")
    }
}

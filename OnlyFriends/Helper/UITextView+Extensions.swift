//
//  UITextView+Extensions.swift
//  SportTime
//
//  Created by Admin on 31/10/20.
//  Copyright © 2020 Gaurav Kive. All rights reserved.
//

import Foundation
import UIKit

extension UITextView{

    func setPlaceholder(_ placeHolder: String?) {

        let placeholderLabel = UILabel()
        placeholderLabel.text = placeHolder//"Enter some text..."
        placeholderLabel.font = UIFont.systemFont(ofSize: (self.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        placeholderLabel.tag = 222
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (self.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !self.text.isEmpty

        self.addSubview(placeholderLabel)
    }

    func checkPlaceholder() {
        let placeholderLabel = self.viewWithTag(222) as! UILabel
        placeholderLabel.isHidden = !self.text.isEmpty
    }
}

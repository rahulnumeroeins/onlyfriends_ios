//
//  ExtensionNSString.swift
//  
//

import Foundation
import UIKit

extension String {
    /**
     *  @return A copy of the receiver with all leading and trailing whitespace removed.
     */
    
    func stringToCommaSepratedDecimal() -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .decimal
        
        return formatter.string(from: NSNumber(value: Double(self)!))!
    }
    
    func stringWithDecimal() -> String {
        let doubleVlaue = Double(self)
        print(String(format: "%.2f", ceil(doubleVlaue!*100)/100))
        return String(format: "%.2f", ceil(doubleVlaue!*100)/100)
        
    }
    
    func msg_stringByTrimingWhitespace() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func trimmedString() -> String {
        return (self.trimmingCharacters(in: (NSCharacterSet.whitespacesAndNewlines)))
    }
    var length: Int {
        return count
    }
    
    func toBase64()->String{
        
        let data = self.data(using: String.Encoding.utf8)
        
        return data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
    }
    
    fileprivate func stringFromResult(_ result: UnsafeMutablePointer<CUnsignedChar>, length: Int) -> String {
        let hash = NSMutableString()
        for i in 0..<length {
            hash.appendFormat("%02x", result[i])
        }
        return BaseApp.sharedInstance.removeOptionalString(String(describing: hash))
    }
    
    
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return BaseApp.sharedInstance.removeOptionalString(String(describing: self[i] as Character))
    }
    
    //    //TODO: change for swift3
    //    subscript (r: Range<Int>) -> String {
    //        // for swift2
    //        //let start = characters.index(startIndex, offsetBy: r.lowerBound)
    //        //let end = start.advancedBy(r.upperBound - r.lowerBound)
    //
    //        // for swift3
    //        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
    //                                            upper: min(length, max(0, r.upperBound))))
    //        let start = index(startIndex, offsetBy: range.lowerBound)
    //        let end = index(start, offsetBy: r.upperBound - r.lowerBound)
    //
    //        return String(self[Range(start ..< end)])
    //    }
    
    func substring(with range: NSRange) -> String {
        let nsString = self as NSString
        return nsString.substring(with: range)
    }
    
    func isEmail() -> Bool {
        if self.contains("..") || self.contains("@@") || self.contains(".@")
            || self.hasPrefix(".") || self.hasSuffix(".con") || self.hasSuffix(".") {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func isContainValidPassword() -> Bool {
        let regularExpression = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#.])[A-Za-z\\d$@$!%*?&#.]{8,20}"
        // let regularExpression = "((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$&*]).{8,20})"
        let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        
        return passwordValidation.evaluate(with: self)
    }
    
    func isValidIFSCNumber() -> Bool {
        let ibanRegEx = "[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}"
        let ibanTest = NSPredicate(format:"SELF MATCHES %@", ibanRegEx)
        return ibanTest.evaluate(with: self)
    }
    
    public var isNumeric: Bool {
        let scanner = Scanner(string: self)
        scanner.locale = NSLocale.current
        return (scanner.scanDecimal() != nil) && scanner.isAtEnd //scanDecimal(nil) && scanner.isAtEnd
    }
    
    public var isDigits: Bool {
        return CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: self))
    }
    
    var hasOnlyNewlineSymbols: Bool {
        return trimmingCharacters(in: CharacterSet.newlines).isEmpty
    }
    
    // MARK - Parse into NSDate
    
    func dateFromFormat(_ format: String) -> Date? {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = format
        return formatter.date(from: self)
    }
    
    var boolValue: Bool {
        return (self as NSString).boolValue
    }
    
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    func stringFrom(_ start: Int, to end: Int? = nil) -> String {
        var maximum = self.count
        
        let i = start < 0 ? self.endIndex : self.startIndex
        let ioffset = min(maximum, max(-1 * maximum, start))
        let startIndex = self.index(i, offsetBy: ioffset)
        
        maximum -= start
        
        let j = end! < 0 ? self.endIndex : self.startIndex
        let joffset = min(maximum, max(-1 * maximum, end ?? 0))
        let endIndex = end != nil && end! < self.count ? self.index(j, offsetBy: joffset) : self.endIndex
        return self.substring(with: (startIndex ..< endIndex))
    }
    func toNSNumber()->NSNumber? {
        
        if let myInteger = Int(self) {
            let myNumber = NSNumber(value:myInteger)
            return myNumber
        }
        return 0
    }
}

extension String{
    func isValidDouble(maxDecimalPlaces: Int) -> Bool {
        let formatter = NumberFormatter()
        formatter.allowsFloats = true // Default is true, be explicit anyways
        let decimalSeparator = formatter.decimalSeparator ?? "."  // Gets the locale specific decimal separator. If for some reason there is none we assume "." is used as separator.
        
        if formatter.number(from: self) != nil {
            let split = self.components(separatedBy: decimalSeparator)
            let digits = split.count == 2 ? split.last ?? "" : ""
            return digits.count <= maxDecimalPlaces    // TODO: Swift 4.0 replace with digits.count, YAY!
        }
        return false // couldn't turn string into a valid number
    }
    
    // MARK: String int check
    var isInt:Bool {
        return Int(self) != nil
    }
    
    func stripOutHtml() -> String? {
        do {
            guard let data = self.data(using: .unicode) else {
                return nil
            }
            let attributed = try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            return attributed.string
        } catch {
            return nil
        }
    }
    
}

extension String {
    //For encoding and decoding Emojies
    func encodeEmoji(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    func decodeEmoji() -> String? {
        let string = self.replacingOccurrences(of: "\\n", with: "\n")
        let data = string.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
}

extension String {
    func image() -> UIImage? {
        let size = CGSize(width: 40, height: 40)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.white.set()
        let rect = CGRect(origin: .zero, size: size)
        UIRectFill(CGRect(origin: .zero, size: size))
        (self as AnyObject).draw(in: rect, withAttributes: [.font: UIFont.systemFont(ofSize: 40)])
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

extension Int {
    func toString()->String? {
        let val = NSNumber(integerLiteral: self)
        return val.stringValue
    }
}

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}

extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
    
    var convertedDictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}

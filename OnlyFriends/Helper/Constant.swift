//
//  Constant.swift
//  PrivateLocator
//
//  Created by Admin on 28/10/20.
//  Copyright © 2020 Gaurav Kive. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import MobileCoreServices
import CoreLocation

enum AppLanguage:String{
    case English
    case Spanish
}

extension AppLanguage {
    var LanguageCode:String{
        switch self {
        case .English:
            return "en"
        case .Spanish:
            return "es"
        }
    }
}

internal var DefaultLang = AppLanguage.English.rawValue

internal let IPHONE_X_DEFAULT_NAVIGATION_BAR_HEIGHT:CGFloat = 88

// IPHONE SCREEN WIDTH AND HEIGHT BOUND CONDITION
let kIS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
let kIS_IPHONE = UIDevice.current.userInterfaceIdiom == .phone
let kIS_RETINA = UIScreen.main.scale >= 2.0
let kSYSTEM_VERSION = Float(UIDevice.current.systemVersion)

let kSCREEN_X = UIScreen.main.bounds.origin.x
let kSCREEN_Y = UIScreen.main.bounds.origin.y
let kSCREEN_WIDTH = UIScreen.main.bounds.size.width
let kSCREEN_HEIGHT = UIScreen.main.bounds.size.height

let kSCREEN_MAX_LENGTH = max(kSCREEN_WIDTH, kSCREEN_HEIGHT)
let kSCREEN_MIN_LENGTH = min(kSCREEN_WIDTH, kSCREEN_HEIGHT)

let kIS_IPHONE_4_OR_LESS = (kIS_IPHONE && kSCREEN_MAX_LENGTH < 568.0)
let kIS_IPHONE_5 = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 568.0)
let kIS_IPHONE_6_OR_7 = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 667.0)
let kIS_IPHONE_6P_OR_7P = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 736.0)
let IS_IPHONE_X_AND_IPHONE_XS = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 812.0)
let IS_IPHONE_XR = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 896.0)
let IS_IPHONE_XS_MAX = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 896.0)

/////////////////////////////////////////////////////////////////////////////////////////////

internal let NUMBERS_ONLY = "1234567890"
internal let DECIMAL_VALUES = "1234567890."
internal let ALPHA_ONLY = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
internal let ALPHA_NUMERIC_ONLY = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
internal let  NUMBER_DECEIMAL_SYMBOLS = "1234567890$."
internal let  NUMBER_DECEIMAL = "1234567890."
internal let  EMAIL_ONLY = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.@"
//USER DEFAULT KEY'S
internal let appLaunchFirstTime = "appLaunchFirstTime"
internal let kDeviceType = "ios"
internal let kAuthorization = "authorization"
internal let kIMG_NAME_PROFPIC = "profile_img.png"
internal let kPROFILE_IMG_KEY = "profile_img"
internal let kRefreshDataOnNetworkConnection = "RefreshDataOnNetworkConnection"
internal let kNotificationCount = "NotificationCount"
internal let kCURRENCY_TEXT = "$"

// Name (Including middle names and surnname) - alpha numeric field max length 30
internal let LIMIT_LOWER_PASSWORD = 8
internal let LIMIT_UPPER_PASSWORD = 20
internal let LIMIT_LOWER_ACCOUNT_NUMBER = 12//5
internal let LIMIT_UPPER_ACCOUNT_NUMBER = 14
internal let LIMIT_LOWER_ACCOUNT_HOLDER = 16
internal let LIMIT_UPPER_ACCOUNT_HOLDER = 20
internal let LIMIT_LOWER_IFSC = 10
internal let LIMIT_UPPER_IFSC = 14

internal let LIMIT_DAY = 2
internal let LIMIT_AMOUNT = 5
internal let LIMIT_NAME = 20
internal let LIMIT_MENU_LINK = 300
internal let LIMIT_ADDRESS = 180
internal let LIMIT_ZIPCODE = 5
internal let LIMIT_EMAIL = 50
internal let LIMIT_PHONE_NUMBER = 10
internal let LIMIT_OTP_NUMBER = 4
internal let LIMIT_LOWER_PHONE_NUMBER = 8
internal let LIMIT_UPPER_PHONE_NUMBER = 10
internal let LIMIT_UPPER_CVV = 3
internal let LIMIT_UPPER_PIN = 4
internal let LIMIT_CARD_NO = 16
internal let LIMIT_CARD_NO_LIMIT = 19

internal let DISCOUNT_PERCENTAGE = 100
internal let AppNotificationButtonTag = 1234321

//Default REST Error Code and Message
internal let DEFAULT_REST_ERROR_CODE = 100
internal let DEFAULT_REST_ERROR_MESSAGE = "Unable to process your request.\nPlease try again later."

/////////////////////////////////////////////////////////////////////////////////////////////
//Random Number Generator for Captcha
func randomString(length: Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return String((0...length-1).map{ _ in letters.randomElement()! })
}

// Make AttributtedString
func makeAttributtedString(title: String, font: UIFont, color: UIColor) -> NSAttributedString {
    // create attributed string
    let myAttribute = [ NSAttributedString.Key.foregroundColor: color,  NSAttributedString.Key.font: font]
    let myAttrString = NSAttributedString(string: title, attributes: myAttribute)
    // return attributed string
    return myAttrString
}

func makeAttributtedStringWithUnderline(title: String, font: UIFont, color: UIColor) -> NSAttributedString {
    // create attributed string
    let myAttribute = [ NSAttributedString.Key.foregroundColor: color,  NSAttributedString.Key.font: font, NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue] as [NSAttributedString.Key : Any]
    let myAttrString = NSAttributedString(string: title, attributes: myAttribute)
    // return attributed string
    return myAttrString
}

//Create image for msolid color
func from(color: UIColor) -> UIImage {
    let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()
    context!.setFillColor(color.cgColor)
    context!.fill(rect)
    let img = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return img!
}

//Create Image with Name Initials with Random color
func imageWith(name: String?, color: UIColor?) -> UIImage? {
    let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
    let nameLabel = UILabel(frame: frame)
    nameLabel.textAlignment = .center
    if color != nil {
        nameLabel.backgroundColor = color
    } else {
        nameLabel.backgroundColor = UIColor(red: CGFloat(drand48()), green: CGFloat(drand48()), blue: CGFloat(drand48()), alpha: 1.0)//.lightGray
    }
    nameLabel.textColor = .white
    nameLabel.font = UIFont.boldSystemFont(ofSize: 40)
    var initials = ""
    if let initialsArray = name?.components(separatedBy: " ") {
        if let firstWord = initialsArray.first {
            if let firstLetter = firstWord.first {
                initials += String(firstLetter).capitalized
            }
        }
        if initialsArray.count > 1, let lastWord = initialsArray.last {
            if let lastLetter = lastWord.first {
                initials += String(lastLetter).capitalized
            }
        }
    } else {
        return nil
    }
    nameLabel.text = initials
    UIGraphicsBeginImageContext(frame.size)
    if let currentContext = UIGraphicsGetCurrentContext() {
        nameLabel.layer.render(in: currentContext)
        let nameImage = UIGraphicsGetImageFromCurrentImageContext()
        return nameImage
    }
    return nil
}

//Create Image with Fixed color
func imageWithFixedColor(name: String?, frame: CGRect?, color: UIColor?, txtColor: UIColor?, txtFont: UIFont?) -> UIImage? {
    let nameLabel = UILabel(frame: frame!)
    nameLabel.textAlignment = .center
    nameLabel.backgroundColor = color
    nameLabel.textColor = txtColor
    nameLabel.font = txtFont
    nameLabel.text = name
    UIGraphicsBeginImageContext(frame!.size)
    if let currentContext = UIGraphicsGetCurrentContext() {
        nameLabel.layer.render(in: currentContext)
        let nameImage = UIGraphicsGetImageFromCurrentImageContext()
        return nameImage
    }
    return nil
}

//For encoding and decoding Emojies
func encodeEmoji(_ s: String) -> String {
    let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
    return String(data: data, encoding: .utf8)!
}

func decodeEmoji(_ s: String) -> String? {
    //let string = s.replacingOccurrences(of: "\\n", with: "\n")
    let data = s.data(using: .utf8)!
    return String(data: data, encoding: .nonLossyASCII)
}

func displayLocationInfo(_ placemark: CLPlacemark?) -> String {
    var labelText = ""
    if let placeMark = placemark {
        //stop updating location to save battery life
        if let name = placeMark.name {
            labelText = name
        }
        if let subThoroughfare = placeMark.subThoroughfare {
            if (subThoroughfare != placeMark.name) && (labelText != subThoroughfare) {
                labelText = (labelText != "") ? labelText + "," + subThoroughfare : subThoroughfare
            }
        }
        if let subLocality = placeMark.subLocality {
            if (subLocality != placeMark.subThoroughfare) && (labelText != subLocality) {
                labelText = (labelText != "") ? labelText + "," + subLocality : subLocality
            }
        }
        if let street = placeMark.thoroughfare {
            if (street != placeMark.subLocality) && (labelText != street) {
                labelText = (labelText != "") ? labelText + "," + street : street
            }
        }
        if let locality = placeMark.locality {
            if (locality != placeMark.thoroughfare) && (labelText != locality) {
                labelText = (labelText != "") ? labelText + "," + locality : locality
            }
        }
        if let city = placeMark.subAdministrativeArea {
            if (city != placeMark.locality) && (labelText != city) {
                labelText = (labelText != "") ? labelText + "," + city : city
            }
        }
        
        if let country = placeMark.country {
            labelText = (labelText != "") ? labelText + "," + country : country
        }
        
        if let postalCode = placeMark.postalCode {
            labelText = (labelText != "") ? labelText + "," + postalCode : postalCode
        }
        // labelText gives you the address of the place
    }
    return labelText
}

func calculateDistanceFromLatLong(_ sourceCoordinate: CLLocation, _ destinationCoordinate: CLLocation) -> String {
    let distance = sourceCoordinate.distance(from: destinationCoordinate) / 1000
    print(String(format: "The distance to my buddy is %.01fkm", distance))
    return String(format: "%.01fkm", distance)
}

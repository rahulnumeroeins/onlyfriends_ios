
import UIKit
import AVFoundation

class Common: NSObject{
    
    static let Title = "Only Freinds"
    static let DeviceType = "ios"
    static var IsCoBorrower = true
    static let Showdateformate = "dd MMM, yyyy"
    static let Senddateformate = "yyyy/MM/dd"
    
    static var Usertype = 1    // 1 = Buyer//  2 = seller  // 3 = ""   // 4 = "" //////
    
    static func validateResponse(_ statusCode: Int) -> Bool {
        if case 200...300 = statusCode {
            return true
        }
        return false
    }
    
    
    class func toString(_ value: Any?) -> String {
        return String(describing: value ?? "")
    }
    
    class func formatPointsInt(from: String) -> String {
        //let value = Int(from)
        let number = Int64(from)
        let thousand = number!  / 1000
        let million = number!  /  1000000
        let billion = number! /   1000000000
        
        if million >= 1 {
            
            let temp = String(format:"%.1f M",(million*10)/10)
            return temp
            
            //return "\((million*10)/10) M"
            //return "\(round(Double(million*10))/10)M"
        } else if thousand >= 1 {
            
            let temp = String(format:"%.1f K",(thousand*10)/10)
            return temp
            
            
            //return "\((thousand*10)/10) K"
            //return "\(round(Double(thousand*10))/10)K"
        } else if billion >= 1 {
            
            let temp = String(format:"%.1f B",(billion*10)/10)
            return temp
            
            //return "\((billion*10)/10) B"
            //return ("\(round(Double(billion*10/10)))B")
        } else {
            return "\(Int64(number!))"
        }
    }
    
    
    
    class func GetDeviceId(){
        //let UUIDValue = UIDevice.current.identifierForVendor!.uuidString
        //SharedPreference.storeDeviceToken(UUIDValue)
    }
    
    // MARK: - Email Validation
    class func isValidEmail(value:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        return NSPredicate(format:"SELF MATCHES %@", emailRegEx).evaluate(with: value)
    }
    
    // MARK: - Number Validation
    class func isValidateNumber(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    // MARK: - Password Validation
    class func isPassword(value:String) -> Bool {
        let passwordRegEx = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,15}$"
        return NSPredicate(format:"SELF MATCHES %@", passwordRegEx).evaluate(with: value)
    }
    
    class func isCheckPasswordLength(password: String)  -> Bool{
        if password.count >= 8{
            return true
        }else{
            return false
        }
    }
    
    class func isCheckOTPLength(password: String)  -> Bool{
        if password.count == 4{
            return true
        }else{
            return false
        }
    }
    
    class func isCheckMobileNumberLength(MobileNumber: String)  -> Bool{
        if MobileNumber.count >= 9{
            return true
        }else{
            return false
        }
    }
    
    class func isCheckPasswordLength(password: String , confirmPassword : String) -> Bool {
        if password.count <= 7 && confirmPassword.count <= 7{
            return true
        }else{
            return false
        }
    }
    
    class func ShowAlert( Title : String, Message : String, VC: UIViewController ){
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alert.addAction(okAction)
        alert.self.show(VC, sender: self)
        VC.present(alert, animated: true, completion: nil)
    }
    
    class func Node(userid : String, otherUserid : String) -> String{
        let Node = String((Int(userid)! * Int(userid)!) + (Int(otherUserid)! * Int(otherUserid)!))
        return Node
    }
    
    // MARK: - get Day date Month From Date String
    class func getDateDetailFrom(strDate:String) -> String{
        if strDate.isEmpty{
            return ""
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "YYYY-MM-dd'T'HH:mm:ss'Z'"
        var date = Date()
        date = dateFormatterGet.date(from: strDate)!
        dateFormatterGet.dateFormat = "EEEE MMM dd yyyy hh-a"
        let strReturn:String = dateFormatterGet.string(from: date)
        return strReturn
    }
    // MARK: - Convert  String To date
    
    class func getDateFromString(strDate:String) -> Date
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSZ"
        // dateFormatterGet.timeZone = TimeZone(identifier: )
        var date = Date()
        date = dateFormatterGet.date(from: strDate)!
        return date
    }
    
    
     class func getBidDateTime(strDate:String) -> String{
          if strDate.isEmpty{
              return ""
          }
          let dateFormatterGet = DateFormatter()
          dateFormatterGet.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSZ"
          var date = Date()
          date = dateFormatterGet.date(from: strDate)!
          dateFormatterGet.dateFormat = "dd-MM-yyyy hh:mm"
          let strReturn:String = dateFormatterGet.string(from: date)
          return strReturn
      }
      
     class func getBidDateOnly(strDate:String) -> String{
          if strDate.isEmpty{
              return ""
          }
          let dateFormatterGet = DateFormatter()
          dateFormatterGet.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSZ"
          var date = Date()
          date = dateFormatterGet.date(from: strDate)!
          dateFormatterGet.dateFormat = "dd MMMM yyyy \n hh:mm a"
          let strReturn:String = dateFormatterGet.string(from: date)
          return strReturn
      }
    
    class func tostring(_ value : Any) -> String{
        return String(describing: value )
    }
    
    class func changecurrencyformate(currency : Int) -> String{
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currencyAccounting
        currencyFormatter.locale = Locale.current
        let priceString = currencyFormatter.string(from: NSNumber(value: currency))!
        return priceString
    }
    
    //  Load viewController from storyboard (used in multi-storyboard)
    class func getViewController(storyboardName:String, viewControllerName:String) -> UIViewController {
        let storyBoard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerName)
        return viewController
    }
    
    ///// Push
    class func PushMethod(VC: UIViewController, identifier : String){
        let Vc = VC.storyboard?.instantiateViewController(withIdentifier: identifier)
        VC.navigationController?.pushViewController(Vc!, animated: true)
    }
    
    ////Pop
    class func PopMethod(VC: UIViewController){
        _ = VC.navigationController?.popViewController(animated: true)
    }
    
    ////Pop
    class func PopRootMethod(VC: UIViewController){
        _ = VC.navigationController?.popToRootViewController(animated: true)
    }
    
    //// Present
    class func PresentMethod(VC: UIViewController, PresentVC : UIViewController){
        VC.present(PresentVC , animated: true, completion: nil)
    }
    
    //// Dismiss
    class func DismissMethod(VC: UIViewController){
        VC.dismiss(animated: true, completion: nil)
    }
    
    class func CheckArray(array : Array<Any>) -> Bool {
        var Available : Bool
        
        if (array.count == 0)  {
            Available = false
        }else if (array.isEmpty){
            Available = false
        }
        else{
            Available = true
        }
        return Available
    }
    
    class func CheckMutableArray(array : NSMutableArray) -> Bool {
        var Available : Bool
        
        if (array.count == 0)  {
            Available = false
        }else{
            Available = true
        }
        return Available
    }
    
    class func getCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let result = formatter.string(from: date)
        return result
    }
    
    class func getCurrentTime() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm a"
        let result = formatter.string(from: date)
        return result
    }
    
    class func ChangeDateFormat(Date: String, fromFormat: String, toFormat: String ) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let newdate = dateFormatter.date(from: Date)
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: newdate!)
    }
    
    
    class func getMessageDate() -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
        return result
    }
    
    ////////////////  Camera Gallery start ///////////////////////
    class func ActionSheetForGallaryAndCamera(Picker : UIImagePickerController, VC: UIViewController) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            /////////// camera //////////
            self.openCamera(Picker: Picker, VC: VC)
            
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            /////////// gallery //////////
            self.openGallary(Picker: Picker, VC: VC)
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        VC.present(alert, animated: true, completion: nil)
        
    }
    
    
    class func openGallary(Picker : UIImagePickerController, VC: UIViewController){
        Picker.allowsEditing = true
        Picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        VC.present(Picker, animated: true, completion: nil)
    }
    
    class func openCamera(Picker : UIImagePickerController, VC: UIViewController){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            Picker.allowsEditing = true
            Picker.sourceType = UIImagePickerController.SourceType.camera
            Picker.cameraCaptureMode = .photo
            VC.present(Picker, animated: true, completion: nil)
        }else{
            // self.ShowAlert(Title: "Camera Not Found", Message: "This device has no Camera", VC: VC)
        }
    }
    
    class func formatOfDateFromServerString(serverStringDate:String) -> String {
        if serverStringDate != "" {
            let dateFormat = DateFormatter()
            dateFormat.timeZone = NSTimeZone.system
            dateFormat.locale = NSLocale.system
            dateFormat.locale = NSLocale.current
            dateFormat.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            dateFormat.formatterBehavior = .default
            let date = dateFormat.date(from: serverStringDate)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            return dateFormatter.string(from: date!)
        }else{
            return "No time available"
        }
    }
    
    class func CalculateHeightAndWidthForText(message:String , width: CGFloat, font: UIFont) -> CGSize {
        let textSize:CGSize
        let textRect:CGSize = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = message.boundingRect(with: textRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        textSize = boundingBox.size
        return textSize
    }
    
    class func decode(_ s: String) -> String? {
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    
    class func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    
    ///////////////////// End ///////////////////////
}

extension String {
    
    public func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "[235689][0-9]{6}([0-9]{3})?"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    
    private func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
    
}

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func buttonAjdjustToFitWidth(button:UIButton) {
        button.titleLabel?.minimumScaleFactor = 0.5
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.textAlignment = .center
    }
    
    func showToast(toastMessage:String){
        //View to blur bg and stopping user interaction
        let bgView = UIView(frame: self.view.frame)
        bgView.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(255.0/255.0), blue: CGFloat(255.0/255.0), alpha: CGFloat(0.0))
        bgView.tag = 555
        
        //Label For showing toast text
        let lblMessage = UILabel()
        lblMessage.numberOfLines = 0
        lblMessage.lineBreakMode = .byWordWrapping
        lblMessage.textColor = .white
        lblMessage.backgroundColor = .black
        lblMessage.textAlignment = .center
        lblMessage.font = UIFont.init(name: "Helvetica Neue", size: 15)
        lblMessage.text = toastMessage
        
        //calculating toast label frame as per message content
        let maxSizeTitle : CGSize = CGSize(width: self.view.bounds.size.width-16, height: self.view.bounds.size.height)
        var expectedSizeTitle : CGSize = lblMessage.sizeThatFits(maxSizeTitle)
        // UILabel can return a size larger than the max size when the number of lines is 1
        
        expectedSizeTitle = CGSize(width:maxSizeTitle.width.getminimum(value2:expectedSizeTitle.width), height: maxSizeTitle.height.getminimum(value2:expectedSizeTitle.height))
        
        lblMessage.frame = CGRect(x:((self.view.bounds.size.width)/2) - ((expectedSizeTitle.width+16)/2) , y: (self.view.bounds.size.height) - (expectedSizeTitle.height+120), width: expectedSizeTitle.width+16, height: expectedSizeTitle.height+16)
        
        lblMessage.layer.cornerRadius = 8
        lblMessage.layer.masksToBounds = true
        lblMessage.padding = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        bgView.addSubview(lblMessage)
        self.view.addSubview(bgView)
        lblMessage.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            lblMessage.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, delay: 2, options: .curveEaseOut, animations: {
                lblMessage.alpha = 0.0
            }, completion: {_ in
                bgView.removeFromSuperview()
            })
        })
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
}

extension CGFloat{
    func getminimum(value2:CGFloat)->CGFloat
    {
        if self < value2{
            return self
        }
        else{
            return value2
        }
    }
}

extension UILabel{
    private struct AssociatedKeys {
        static var padding = UIEdgeInsets()
    }
    var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
}

extension UIColor {
    
    class func hexString(hex: String, alpha: CGFloat? = 1) -> UIColor {
        return UIColor.hexStr(Str: hex, alpha: alpha!)
    }
    
    class private func hexStr(Str: String, alpha: CGFloat) -> UIColor {
        let hexStr = Str.replacingOccurrences(of: "#", with: "")
        let scanner = Scanner(string: hexStr)
        var color: UInt32 = 0
        if scanner.scanHexInt32(&color) {
            let r = CGFloat((color & 0xFF0000) >> 16) / 255.0
            let g = CGFloat((color & 0x00FF00) >> 8) / 255.0
            let b = CGFloat(color & 0x0000FF) / 255.0
            return UIColor(red:r,green:g,blue:b,alpha:alpha)
        } else {
            print("invalid hex string", terminator: "")
            return UIColor.white;
        }
    }
}

extension UITextField {
    @IBInspectable var placeholderColor: UIColor {
        get {
            return attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? .clear
        }
        set {
            guard let attributedPlaceholder = attributedPlaceholder else { return }
            let attributes: [NSAttributedString.Key: UIColor] = [.foregroundColor: newValue]
            self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: attributes)
        }
    }
}


extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y);
        var indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        indexOfCharacter = indexOfCharacter + 4
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

protocol MyPickerViewProtocol {
    func myPickerDidSelectRow(Index:Int?, Tag:Int)
}

class MyPickerView: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    
     var MyPickerDelegate:MyPickerViewProtocol?
     var  arrItems  = [String]()
     var Ctag : Int = 0
    
    
    convenience init(withDictionary response:[String], Tag: Int) {
        self.init()
        self.arrItems = response
        self.Ctag = Tag
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrItems.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrItems[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.MyPickerDelegate?.myPickerDidSelectRow(Index: row, Tag: self.Ctag)
    }
}

public extension NSObject{
    
    class var nameOfClass: String{
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    var nameOfClass: String{
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }
}

//
//  ViewController.swift
//  OnlyFriends
//
//  Created by NumeroEins on 23/02/21.
//  Copyright © 2021 NumeroEins. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
}

extension ViewController {
    @IBAction func action_Login(_ sender: UIButton) {
        let vc = LoginVC.instantiate(fromStoryboard: .OnBoard)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func action_SignUp(_ sender: UIButton) {
//        let vc = SignUpVC.instantiate(fromStoryboard: .OnBoard)
//        self.navigationController?.pushViewController(vc, animated: true)
        sceneDelegate.openDashboardViewController()
    }
}
